package com.unipi.talepis;

import java.util.Scanner;

public class Main {
    private static Boolean work = false;

    public static void main(String[] args) {
	   Thread t1 =  new Thread(()->{
	        while (getWork()!=true){
                try {
                    Thread.sleep(2000);
                    System.out.println("waiting...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // do working
           System.out.println("I worked!");
           System.out.println("Now I am dying...");
           setWork(false);
        });
	   t1.setDaemon(true);
	   t1.start();
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        while (!s.equals("exit")){
            if (s.equals("yes"))
                setWork(true);
            s = scanner.next();
        }
    }

    public static Boolean getWork() {
        return work;
    }

    public synchronized static void setWork(Boolean work) {
        Main.work = work;
    }
}
