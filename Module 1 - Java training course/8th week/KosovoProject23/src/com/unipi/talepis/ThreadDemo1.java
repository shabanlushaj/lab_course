package com.unipi.talepis;

import java.util.Scanner;

public class ThreadDemo1 {
    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        t1.setDaemon(true);
        t1.start();
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Write something");
            String fromUser = scanner.nextLine();
            if (fromUser.equals("exit"))
                return;
            else if (fromUser.contains("new")){
                String message = fromUser.replace("new ","");
                t1.setMessage(message);
            }else {
                System.out.println("You just wrote: "+fromUser);
            }
        }
    }
}

class MyThread extends Thread{
    @Override
    public void run() {
        while (true){
            if (message!=null){
                if (message.equals("exit")){
                    System.out.println("Thread is exiting!...");
                    return;
                }
                processMessages(message);
                message = null;
            }
            try{
                Thread.sleep(600000);
            } catch (InterruptedException e) {
                if (e instanceof InterruptedException)
                    continue;
            }
        }
    }

    private String message;

    public synchronized void setMessage(String message) {
        this.message = message;
        interruptThread();
    }
    private void interruptThread(){
        this.interrupt();
    }
    private void processMessages(String message){
        System.out.println("I am doing something with:"+message);
    }
}
