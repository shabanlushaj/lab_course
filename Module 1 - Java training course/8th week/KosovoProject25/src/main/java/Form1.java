import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Form1 extends JFrame{
    private JPanel panel1;
    private JTextField textField1;
    private JButton readButton;
    private JButton writeButton;
    private JList list1;
    private JLabel label1;
    private JButton readAllTheTimeButton;
    private JButton addButton;
    FirebaseDatabase database;
    DatabaseReference ref,ref2;
    public Form1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setContentPane(panel1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        DefaultListModel listModel = new DefaultListModel();
        list1.setModel(listModel);
        try
        {
            FileInputStream serviceAccount =
                    new FileInputStream("src/main/resources/firebasekey/kosovoproject25-545b3-firebase-adminsdk-opacf-c498746c81.json");
            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://kosovoproject25-545b3-default-rtdb.firebaseio.com/")
                    .build();
            FirebaseApp.initializeApp(options);
            database = FirebaseDatabase.getInstance();
            ref = database.getReference("message");
            ref2 = database.getReference("messages");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ref.setValue(textField1.getText(), (databaseError, databaseReference) -> {
                    if (databaseError!=null){
                        label1.setText("Data could not be saved "+databaseError.getMessage());
                    }else {
                        label1.setText("Data saved successfully!");
                        textField1.setText("");
                    }
                });
            }
        });
        readButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        listModel.addElement(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        label1.setText(databaseError.getMessage());
                    }
                });
            }
        });
        readAllTheTimeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        listModel.addElement(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        label1.setText(databaseError.getMessage());
                    }
                });
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ref2.push().setValue(textField1.getText(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError!=null){
                            label1.setText("Data could not be saved "+databaseError.getMessage());
                        }else {
                            label1.setText("Data saved successfully!");
                            textField1.setText("");
                        }
                    }
                });
            }
        });
    }

    public static void main(String[] args) {
        new Form1();
    }
}
