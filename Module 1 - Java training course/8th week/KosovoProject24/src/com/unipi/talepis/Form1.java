package com.unipi.talepis;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Form1 extends JFrame{
    private JPanel panel1;
    Graphics2D g;
    Point previousPoint;
    int startX,startY,endX,endY;
    public Form1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setContentPane(panel1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        g = (Graphics2D)panel1.getGraphics();
        g.setStroke(new BasicStroke(1));
        previousPoint = new Point();

        panel1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                BufferedImage img = null;
                try{
                    img = ImageIO.read(new File("E:\\Dropbox\\java\\intellijprojects\\KosovoProject24\\src\\com\\unipi\\talepis\\sun.png"));
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                g.clearRect(previousPoint.x,previousPoint.y,50,50);
                g.drawImage(img,e.getX(),e.getY(),50,50,null);
                previousPoint.x = e.getX();
                previousPoint.y = e.getY();
                //drawSmallCircle(e.getX(),e.getY());
                //movingCircleAtPoint(e.getX(),e.getY());
            }
        });

        panel1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                startX = e.getX();
                startY = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                endX = e.getX();
                endY = e.getY();
                movingCircle(startX,startY,endX,endY);
            }
        });
    }
    private void movingCircle(int x1, int y1, int x2, int y2){
        new Thread(new Runnable() {
            final double distance = Math.sqrt(Math.pow((x1-x2),2)+Math.pow((y1-y2),2));
            final double step = distance/40;
            final double radius = Math.atan2((y2-y1),(x2-x1));
            @Override
            public void run() {
                for (int i=1;i<41;i++){
                    int nextX = (int)(x1+i*step*Math.cos(radius));
                    int nextY = (int)(y1+i*step*Math.sin(radius));
                    drawSmallCircle(nextX,nextY);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    private void movingCircleAtPoint(int x, int y){
        new Thread(new Runnable() {
            int step = 0;
            @Override
            public void run() {
                for (int i=0;i<40;i++){
                    drawSmallCircle(x+step,y+step);
                    step+=7;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    private void drawSmallCircle(int x, int y){
        g.clearRect(previousPoint.x-5,previousPoint.y-5,10,10);
        g.setColor(Color.RED);
        g.fillOval(x-5,y-5,10,10);
        previousPoint.x = x;
        previousPoint.y = y;
    }
}
