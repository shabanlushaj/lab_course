package com.banti.slide17_23;

public class Tester {

    public static void main(String[] args)
    {
        //Create 3 threads - one is the main thread, one by implementing Runnable and one by extending Thread
        Tester testObj = new Tester();
        RunnableClass runObj = new RunnableClass("Implemented thread");
        Thread threadObj = new Thread(runObj);
        MyThread myThreadObj = new MyThread("Extended thread");

        //Get thread information
		/*
		Thread t = Thread.currentThread();
		int numThreads = Thread.activeCount();

		System.out.println("Number of threads = " + numThreads);
		System.out.println("\tCurrent thread = "+ t);
		System.out.println("\tName = " + t.getName());
		*/


        myThreadObj.run();
        threadObj.start();
        testObj.runnableTest();
        System.out.println(numberOfThreads());
    }

    public void runnableTest()
    {
        for(int i = 0; i < 30; i ++)
        {
            System.out.println("Main thread - " + i);
        }
    }

    public static int numberOfThreads()
    {
        Thread t = Thread.currentThread();
        int numThreads = Thread.activeCount();
        return numThreads;
    }

}
