package com.banti.slide17_23;

public class RunnableClass implements Runnable{

    String message;

    public RunnableClass(String aMessage)
    {
        this.message = aMessage;
    }

    public void run()
    {
        printLoop();
    }

    public void printLoop()
    {
        for(int i = 0; i < 10; i ++)
        {
            System.out.println(message + i);
        }
    }

}