package com.banti.slide26;

public class Main {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable(), "First thread");
        Thread t2 = new Thread(new MyRunnable(), "Second thread");
        Thread t3 = new Thread(new MyRunnable(), "Third thread");

        t1.start();

        try {
            t1.join(500);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        t2.start();

        try {
            t1.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Threads are done!!!!");
    }
}


class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println("Current thread: " + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Thread finished: "+Thread.currentThread().getName());
    }
}