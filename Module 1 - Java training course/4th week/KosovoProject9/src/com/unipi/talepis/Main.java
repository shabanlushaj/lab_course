package com.unipi.talepis;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.s = "blah";
        Class<MyClass> myClassC = MyClass.class;
        readAnnotation(myClassC);
        Method method = null;
        try {
            method = myClassC.getMethod("doSomething");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        readAnnotation(method);
    }
    //@Override --compile error
    void myMethod(){

    }

    @Override
    public String toString() {
        return "boo";
    }

    static void readAnnotation(AnnotatedElement element){
        System.out.println("Finding annotations inside "+element.getClass().getName());
        Annotation[] annotations = element.getAnnotations();
        for (Annotation annotation : annotations){
            if (annotation instanceof JavaFileInfo){
                JavaFileInfo fileInfo = (JavaFileInfo)annotation;
                System.out.println("Info: Author="+fileInfo.author());
                System.out.println("Info: Version="+fileInfo.version());
            }
        }
    }

}
