package com.unipi.talepis;

import java.io.*;

public class SerializeClass {
    public static void main(String... args){
        /*Student s1 = new Student();
        s1.setAge(20);
        s1.setEmail("s1@email.com");
        s1.setName("Blerta");
        s1.setSecret("12er34");
        customSerialize(s1,"s1.ser");
        System.out.println("Done!");*/
        Student s1_clone = customDeserialize("s1.ser");
        System.out.println(s1_clone.toString());
    }
    static void customSerialize(Student s, String filename){
        try {
            FileOutputStream fileOut = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(s);
            out.close();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static Student customDeserialize(String filename){
        Student student = null;
        try {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            student=(Student)in.readObject();
            in.close();
            fileIn.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return student;
    }
}
