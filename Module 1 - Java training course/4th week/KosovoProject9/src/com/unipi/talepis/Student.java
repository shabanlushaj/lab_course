package com.unipi.talepis;

import java.io.Serializable;

public class Student implements Serializable {
    private int age;
    private String name;
    private String email;
    private transient String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Name:"+this.name+"\nemail:"+this.email+"\nAge:"+this.age+"\nSecret:"+this.secret;
    }
}
