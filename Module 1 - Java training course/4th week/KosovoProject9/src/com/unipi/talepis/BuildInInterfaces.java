package com.unipi.talepis;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class BuildInInterfaces {
    public static void main(String... args){


        //Predicates
        Predicate<String> predicate = (s -> s.length()>0);
        System.out.println(predicate.test(""));;
        System.out.println(predicate.test("John"));
        System.out.println(predicate.negate().test("Hi"));

        Predicate<String> isEmptyV2 = String::isEmpty;

        //Functions
        Function<String,Integer> convertToInt = Integer::valueOf;
        convertToInt.apply("146");

        //Suppliers
        Supplier<Student> studentSupplier = Student::new;
        studentSupplier.get().setSecret("54grtef");
        studentSupplier.get().getAge();

        //Consumers
        Consumer<Student> helloStudent =
                (student -> System.out.println("Hello "+student.getName()));
        Student s2 = new Student();
        s2.setName("Visar");
        helloStudent.accept(s2);
        helloStudent.accept(new Student());

        //Comparators
        Comparator<Student> studentComparator =
                (st1,st2)->st1.getName().compareTo(st2.getName());
        //your example with students!
    }
}
