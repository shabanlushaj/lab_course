package com.unipi.talepis;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class Main {

    public static void main(String[] args) {
	    Class c = MyClass.class;
	    MyClass obj = new MyClass(123456);
        Field[] fields = c.getFields();
        Field[] declaredFields = c.getDeclaredFields();
        System.out.println("All accessible field of "+c.getName());
        for (Field field:
             declaredFields) {
            Class type = field.getType();
            String name = field.getName();
            String modifiers = Modifier.toString(field.getModifiers());
            System.out.println(modifiers+" "+type.getSimpleName()+" "+name+";");
        }
        System.out.println("All fields of class "+c.getName());
        for (Field field :
                fields) {
            Class type = field.getType();
            String name = field.getName();
            String modifiers = Modifier.toString(field.getModifiers());
            System.out.println(modifiers+" "+type.getSimpleName()+" "+name+";");
        }
        //Constructors
        Constructor[] constructors = c.getConstructors();
        Class[] parameterTypes = constructors[1].getParameterTypes();
        //Methods
        Method[] methods = c.getMethods();
        Class[] methodParameterTypes = methods[0].getParameterTypes();
        Class returnType = methods[0].getReturnType();
        try {
            Method method = c.getMethod("speak", String.class);
            method.invoke(obj,"Kosovo");
            Method method2 = c.getDeclaredMethod("addTwoNumbers", int.class, int.class);
            method2.setAccessible(true); //be very carefull!!!
            int sum = (int) method2.invoke(null,10,5);
            System.out.println(sum);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //Get Annotations
        Annotation[] annotations = c.getAnnotations();
    }
}
