package com.unipi.talepis;

class MyClass extends MySuperClass implements ISpeak{
    public int tel;
    public String email;
    private String secret;

    public void setTel(int tel) {
        this.tel = tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecret() {
        return secret;
    }

    public MyClass(int tel) {
        this.tel = tel;
    }

    public MyClass(int tel, String secret) {
        this.tel = tel;
        this.secret = secret;
    }

    private static int addTwoNumbers(int a, int b){
        return a+b;
    }

    @Override
    public void speak(String message) {
        System.out.println("Yess I am speaking:"+message);
    }
}

