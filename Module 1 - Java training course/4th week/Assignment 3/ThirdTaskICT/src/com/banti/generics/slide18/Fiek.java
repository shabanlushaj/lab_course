package com.banti.generics.slide18;

public class Fiek<T extends AI>{
    private T obj;
    Fiek(T obj)
    {
        this.obj=obj;
    }
    public void displayObj()
    {
        obj.display();
    }
}


class Engineering implements AI, CN {
    public void display() {
        System.out.println("Name: Engineering");
    }

    @Override
    public void Modules(String[] modules) {
        for (String mod : modules)
            System.out.println(mod);
    }
}

class Cse extends Engineering{
    private static final int deptId = 270;
    private static final String name="Computer Science and Engineering";
    private static final int duration=3;

    public void display()
    {
        System.out.printf("Name: %s DepId: %d Duration: %d\n",name,deptId,duration);
    }

    @Override
    public void Modules(String[] modules) {
        for(String mod : modules)
            System.out.println(mod);
    }
}
interface AI{
    final String[] nodes = {"Machine Learning","Databases","Software Engineering"};
    void Modules(String[] modules);
    default void display(){
        System.out.println("AI");
    }
}
interface CN{
    final String[] modules = {"Data Security","Computer Networks","Internet Security"};
    void Modules(String[] modules);
    default void display(){
        System.out.println("AI");
    }
}

class Main{
    public static void main(String[] args) {
        Fiek<Cse> cseFiek = new Fiek<>(new Cse());
        cseFiek.displayObj();

        Fiek<Engineering> engineeringFiek = new Fiek<>(new Engineering());
        engineeringFiek.displayObj();

        Fiek<AI> artificialI = new Fiek<>(new Cse());
        artificialI.displayObj();

        Fiek<Engineering> eng = new Fiek<>(new Engineering());
        eng.displayObj();

        // Fiek<CN> it'll throw an error;
    }
}
