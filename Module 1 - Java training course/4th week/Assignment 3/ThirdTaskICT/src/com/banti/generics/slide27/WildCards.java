package com.banti.generics.slide27;

import java.util.ArrayList;
import java.util.List;

public class WildCards {
    public static void main(String[] args) {
        Object o;

        ArrayList<? extends Number> numList;
        numList = new ArrayList<Integer>();
        //numList = new ArrayList<Number>() throw an error ^ can't get the upper bond
        ArrayList<Integer> intList = new ArrayList<>();
        for (int j = -5; j <=5 ; j++) {
            intList.add(j);
        }
        numList = intList; // it wont throw an error;

        //but if we try
        //intList = numList.get(1); it will throw an error because intList(is lower) can't get f.e. <double> or <float> values

        o = numList.get(4); //it won't throw an error cause literally list is upper

        //super keyword
        ArrayList<? super Integer> intSuper;
        intSuper = new ArrayList<Integer>();
        // intSuper = new Object() //can't go lower bond in a super keyword

        ArrayList<Number> num = new ArrayList<>();
        copyTo(num,intList); //num is Number list , intList is Integer list

    }
    public static <T> void copyTo(List<? super T> dst, List<? extends T> src){
        //use extends to get values ; use super to put values
        dst.addAll(src);
        System.out.println(dst);
    }
}
