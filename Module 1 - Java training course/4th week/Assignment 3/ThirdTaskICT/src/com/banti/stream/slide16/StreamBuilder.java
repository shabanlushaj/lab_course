package com.banti.stream.slide16;

import java.util.stream.Stream;

public class StreamBuilder {
    public static void main(String[] args) {
        Stream.Builder<String> builder = Stream.builder();

        builder.add("one").add("two").add("three");
        builder.accept("four");

        Stream<String> stream1 = builder.build();
        stream1.forEach(System.out::println);
    }
}
