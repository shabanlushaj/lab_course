package com.banti.stream.slide19;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class StreamReference {
    public static void main(String[] args) {
        List<Person> list = List.of(
                new Person("John","Doe",30),
                new Person("Hello","Example",34),
                new Person("Name","Surname",18)
        );
        Stream<Person> stream = list.stream();
        //findFirst()
        Optional<Person> optional1 = stream.findFirst();

        //findAny()
        //Optional<Person> optional2 = stream.findAny(); //Stream can't be re-used

        list.stream()
                .map(Person::age)
                .filter(age -> age > 20)
                .forEach(System.out::println);
    }
}
