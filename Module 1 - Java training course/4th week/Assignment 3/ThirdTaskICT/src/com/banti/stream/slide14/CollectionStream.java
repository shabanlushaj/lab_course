package com.banti.stream.slide14;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionStream {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 1, 1, 2, 3, 4, 5, 7, 8, 10);

        // Convert List to Set containing numbers less than 7

        Set<Integer> set = numbers.stream()
                .filter(n -> n < 7)
                .collect(Collectors.toSet());

        System.out.println(set);

        List<String> names = Arrays.asList("John", null, "Doe", "Example", null, "Test");

        // Convert name list to Map containing (name -> lengthOf(name))
        Map<String, Integer> map = names.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(name -> name, String::length));

        System.out.println(map);
        System.out.println("############");

        List<Student> students = List.of(
                new Student("Monica","Database",8),
                new Student("Doe","Database",7),
                new Student("Doe","Math",8),
                new Student("Ally","Database",7),
                new Student("Monica","Math",9),
                new Student("Ally","Math",9));

        // Find average grade for each lesson by student
        Map<String, Map<String, Double>> studentAverage = students.stream()
                .collect(Collectors.groupingBy(Student::name,
                        Collectors.groupingBy(Student::course,
                                Collectors.averagingInt(Student::grade)
                        )
                ));
        System.out.println(studentAverage);
    }
}
