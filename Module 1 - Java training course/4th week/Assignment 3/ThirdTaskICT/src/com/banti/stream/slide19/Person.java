package com.banti.stream.slide19;

public record Person(String name, String surname, int age) {
}
