package com.banti.stream.slide17;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public class StreamPattern {
    public static void main(String[] args) {
        Stream<String> words = Pattern.compile("[1-9]").splitAsStream("Hello1World2Example3John4");
        words.forEach(System.out::println);
    }
}
