package com.unipi.talepis;

import java.util.Random;

public class Main implements Runnable {
    static String var ="Blerta";
    private Random rand = new Random(System.currentTimeMillis());

    public void run () {
        //createRandomInts();
        synchronized (var) {
            for (int j = 0; j < 100; j++) {
                System.out.print(Thread.currentThread().getId());
            }
        }
    }
    void createRandomInts(){
        for (int i = 0; i< 100000000; i++) {
            rand.nextInt();
        }
        System.out.println("[" + Thread.currentThread().getName()+"] finished");
    }

    public static void main(String[] args) throws InterruptedException {
        Thread[] threads = new Thread[5];
        for (int i=0; i < threads.length; i++) {
            threads[i] = new Thread(new Main(), "joinThread-" + i);
            //threads[i].setDaemon(true);
            threads[i].start();
        }
        for (int i =0; i<threads.length; i++) {
            threads[i].join();
        }
        //Thread.sleep(3000);
        System.out.println("[" + Thread.currentThread().getName() +"] All threads have finished");
    }
}

