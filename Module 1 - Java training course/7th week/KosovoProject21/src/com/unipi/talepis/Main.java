package com.unipi.talepis;

public class Main {

    public static void main(String[] args) {
	    Thread t1 = new Thread(()->{
	        printChar100Times('A');
        });
	    t1.start();
        Thread t2 = new Thread(()->{
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printChar100Times('B');
        });
        t2.start();
        new Thread(()->{
            try {
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printChar100Times('C');
        }).start();
    }
    public static void printChar100Times(char c) {
        for(int i=0;i<100;i++){
            System.out.print(c);
        }
    }
}
