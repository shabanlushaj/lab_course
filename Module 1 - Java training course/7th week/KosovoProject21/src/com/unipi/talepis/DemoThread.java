package com.unipi.talepis;

import java.util.concurrent.locks.ReentrantLock;

public class DemoThread {
    static ReentrantLock lock = new ReentrantLock();
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            lock.lock(); //try to lock
            printChar100Times('A'); //critical part of code
            lock.unlock(); //unlock
        });
        t1.start();
        Thread t2 = new Thread(()->{
            lock.lock();
            printChar100Times('B');
            lock.unlock();
        });
        t2.start();
        new Thread(()->{
            lock.lock();
            printChar100Times('C');
            lock.unlock();
        }).start();
    }
    public static void printChar100Times(char c) {
        for(int i=0;i<100;i++){
            System.out.print(c);
        }
    }
}
