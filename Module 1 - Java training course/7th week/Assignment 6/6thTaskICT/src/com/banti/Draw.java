package com.banti;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Draw extends JFrame{
    private JPanel panel;
    private JButton PICKCOLORButton;
    private JButton RECTANGLEButton;
    private JButton freePenButton;
    private JButton STRAIGHTLINEButton;
    private JButton CIRCLEButton;
    private JButton CLEARButton;
    private JPanel panel1;
    private JComboBox comboBox1;
    private JButton WIDTHButton;
    private int comboWidth;
    Graphics2D g;
    private Color color;
    Point previousPoint;
    private int stroke;
    boolean draw,line,circle,rectangle = false;
    int prevX,prevY;

    public Draw()
    {
        setTitle("Drawing application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500,500));
        setContentPane(panel);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        g = (Graphics2D)panel1.getGraphics();

        //draw free line
        freePenButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                line = false;
                circle = false;
                rectangle = false;
                draw = true;
//                super.mouseClicked(e);
                previousPoint = new Point();
                panel1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        g.setStroke(new BasicStroke(Integer.parseInt(comboBox1.getSelectedItem().toString())));
                        g.setColor(color);
                        prevX = e.getX();
                        prevY = e.getY();
                    }
                });
                panel1.addMouseMotionListener(new MouseMotionAdapter() {
                    @Override
                    public void mouseDragged(MouseEvent e) {
                        if (draw){
                            g.drawLine(prevX,prevY,e.getX(),e.getY());
                            prevX = e.getX();
                            prevY = e.getY();
                        }
                    }
                });
            }
        });
        PICKCOLORButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color2 = JColorChooser.showDialog(Draw.this,"Please choose a color",panel.getBackground());
                color = color2;
            }
        });
        STRAIGHTLINEButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                line = true;
                circle = false;
                rectangle = false;
                draw = false;
                panel1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {

                        prevX = e.getX();
                        prevY = e.getY();
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if(line) {
                            g.setStroke(new BasicStroke(Integer.parseInt(comboBox1.getSelectedItem().toString())));
                            g.setColor(color);
                            g.drawLine(prevX, prevY, e.getX(), e.getY());
                        }
                    }
                });

            }
        });

        RECTANGLEButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                line = false;
                circle = false;
                rectangle = true;
                draw = false;
                panel1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        prevX = e.getX();
                        prevY=e.getY();
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if (rectangle) {
                            g.setStroke(new BasicStroke(Integer.parseInt(comboBox1.getSelectedItem().toString())));
                            g.setColor(color);
                            if (e.getX() - prevX < 0 && e.getY() - prevY < 0)
                                g.drawRect(e.getX(), e.getY(), prevX-e.getX(), prevY-e.getY());

                            else if(e.getX()-prevX<0&&e.getY()-prevY>0)
                                g.drawRect(e.getX(), prevY, prevX-e.getX(), e.getY()-prevY);

                            else if(e.getX()-prevX>0&&e.getY()-prevY<0)
                                g.drawRect(prevX, e.getY(), e.getX()-prevX, prevY-e.getY());

                            else
                                g.drawRect(prevX, prevY, e.getX() - prevX, e.getY() - prevY);
                        }
                    }
                });
            }
        });
        CIRCLEButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                line = false;
                circle = true;
                rectangle = false;
                draw = false;
                panel1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        prevX = e.getX();
                        prevY = e.getY();
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if (circle) {
                            g.setStroke(new BasicStroke(Integer.parseInt(comboBox1.getSelectedItem().toString())));
                            g.setColor(color);
                            if (e.getX() - prevX < 0 && e.getY() - prevY < 0)
                                g.drawOval(e.getX(), e.getY(), prevX-e.getX(), prevY-e.getY());

                            else if(e.getX()-prevX<0&&e.getY()-prevY>0)
                                g.drawOval(e.getX(), prevY, prevX-e.getX(), e.getY()-prevY);

                            else if(e.getX()-prevX>0&&e.getY()-prevY<0)
                                g.drawOval(prevX, e.getY(), e.getX()-prevX, prevY-e.getY());

                            else
                                g.drawOval(prevX, prevY, e.getX() - prevX, e.getY() - prevY);
                        }
                    }
                });
            }
        });
        CLEARButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                 panel1.removeAll();
                 revalidate();
                 repaint();
            }
        });

    }
}
