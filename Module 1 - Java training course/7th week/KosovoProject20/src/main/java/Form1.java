import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;

public class Form1 extends JFrame{
    private JPanel panel1;
    private JTextArea textArea1;
    private JButton openFileButton;
    private JButton saveFileButton;
    private JButton chooseColorButton;
    private JLabel pictureLabel;
    private JButton loadImageButton;

    public Form1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setContentPane(panel1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        openFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files","txt");
                fileChooser.addChoosableFileFilter(filter);
                if (fileChooser.showOpenDialog(Form1.this)==JFileChooser.APPROVE_OPTION){
                    File textFile = fileChooser.getSelectedFile();
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader(textFile));
                        String s;
                        StringBuilder builder = new StringBuilder();
                        while ((s=reader.readLine())!=null){
                            builder.append(s).append("\n");
                        }
                        textArea1.setText(builder.toString());
                    } catch (FileNotFoundException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        });
        saveFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Text files","txt");
                fileChooser.addChoosableFileFilter(filter);
                if (fileChooser.showSaveDialog(Form1.this)==JFileChooser.APPROVE_OPTION){

                    try {
                        PrintWriter out = new PrintWriter(fileChooser.getSelectedFile().getAbsolutePath()+".txt");
                        out.println(textArea1.getText());
                        out.close();
                    } catch (FileNotFoundException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    }
                    JOptionPane.showMessageDialog(Form1.this,"File saved!");
                }
            }
        });
        chooseColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color = JColorChooser.showDialog(Form1.this,"Please choose a color",panel1.getBackground());
                panel1.setBackground(color);
            }
        });
        loadImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               JFileChooser fileChooser = new JFileChooser();
               fileChooser.setAcceptAllFileFilterUsed(false);
               FileNameExtensionFilter filter = new FileNameExtensionFilter("Image files","jpg","png");
               fileChooser.addChoosableFileFilter(filter);
               if (fileChooser.showOpenDialog(Form1.this)==JFileChooser.APPROVE_OPTION){
                   String imagePath = fileChooser.getSelectedFile().getAbsolutePath();
                   pictureLabel.setText("");
                   loadImage(imagePath,pictureLabel);
               }
            }
        });
    }

    private void loadImage(String path, JLabel pictureLabel){
        pictureLabel.setIcon(null);
        try {
            BufferedImage img = ImageIO.read(new File(path));
            Image resized = img.getScaledInstance(pictureLabel.getWidth(),pictureLabel.getHeight(),
                    Image.SCALE_SMOOTH);
            pictureLabel.setIcon(new ImageIcon(resized));
            pictureLabel.revalidate();
            pictureLabel.repaint();
            pictureLabel.update(pictureLabel.getGraphics());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Form1();
    }
}
