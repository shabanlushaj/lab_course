package com.unipi.talepis;

import java.sql.*;

public class Mysql{

    public static void main(String[] args) {
        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:mysql://localhost:3306/demo2"
                    ,"account","password");
            Statement statement = connection.createStatement();
            String createSQL = "CREATE TABLE IF NOT EXISTS STUDENT2" +
                    "(ID INTEGER NOT NULL," +
                    "NAME VARCHAR(50)," +
                    "GRADE INTEGER," +
                    "PRIMARY KEY (ID))";
            statement.executeUpdate(createSQL);
            String insertSQL = "INSERT INTO STUDENT2 " +
                    "VALUES(2,'Dielleza',8)";
            statement.executeUpdate(insertSQL);
            String updateSQL = "UPDATE STUDENT2 " +
                    "SET GRADE=9 " +
                    "WHERE ID in (1,2)";
            statement.executeUpdate(updateSQL);
            String selectSQL = "SELECT NAME,GRADE FROM STUDENT2";
            ResultSet rs = statement.executeQuery(selectSQL);
            StringBuilder sb = new StringBuilder();
            while (rs.next()){
                sb.append(rs.getString("NAME"))
                        .append(",")
                        .append(rs.getInt("GRADE"))
                        .append("\n");
            }
            System.out.println(sb.toString());
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
