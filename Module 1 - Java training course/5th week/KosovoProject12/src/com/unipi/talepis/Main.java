package com.unipi.talepis;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:h2:~/kosovoDB1");
            PreparedStatement select = connection.prepareStatement("Select * from temp1");
            ResultSet resultSet = select.executeQuery();
            StringBuilder builder = new StringBuilder();
            while (resultSet.next()){
                builder.append(resultSet.getString("name"))
                        .append("\n");
            }

            connection.close();
            System.out.println(builder.toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
