package com.unipi.talepis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Normalizer;

public class Form1 extends JFrame {
    public JPanel panel1;
    private JTextField textField1;
    private JButton button1;
    private JLabel label1;
    private JButton button2;

    public Form1() {
        setTitle("Form1");
        setPreferredSize(new Dimension(500,400));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label1.setText(textField1.getText());
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Form1.this.setVisible(false);
                new Form2();
            }
        });
    }
}
