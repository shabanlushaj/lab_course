package com.unipi.talepis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form2 extends JFrame {
    private JPanel panel1;
    private JCheckBox checkBox1;

    public Form2() {
        setTitle("Form2");
        setPreferredSize(new Dimension(600,400));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
