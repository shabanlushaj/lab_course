package com.banti.annotations;

@TestInfo(priority = TestInfo.Priority.HIGH, createdBy = "John",tags = {"information","data"})
public class TestInfoExample {
    @Test
    void testA() {
        if (true) {
            throw new RuntimeException("This test always failed");
        }
    }

    @Test(enabled = false)
    void testB() {
        if (false)
            throw new RuntimeException("This test always passed");
    }

    @Test
    void testC() {
        if (10 > 1) {
            // do nothing, this test always passed.
        }
    }
}
