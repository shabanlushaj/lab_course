package com.banti.annotations;

public class Main {
    public static void main(String[] args) {
        Class<TestInfoExample> obj = TestInfoExample.class;

        getTesterInfo(obj);
    }
    private static void getTesterInfo(Class<TestInfoExample> obj) {
        if (obj.isAnnotationPresent(TestInfo.class)) {
            TestInfo testerInfo = obj.getAnnotation(TestInfo.class);

            System.out.printf("%nPriority: %s", testerInfo.priority());
            System.out.printf("%nCreatedBy: %s", testerInfo.createdBy());
            System.out.printf("%nTags: ");

            for (String tag : testerInfo.tags()) {
                System.out.print(tag+" ");
            }
        }
    }
}
