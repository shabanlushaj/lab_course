package com.banti.reflection.slide24;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Fields {
    public static void main(String[] args) {
        Class test = B.class;
        allFields(test);
        System.out.println("#####");
        fields(test);
    }
    //it will print all the fields declared as public(also from super class)
    static void allFields(Class cls)
    {
        Field[] fields = cls.getFields();
        for (Field field : fields)
        {
            System.out.println(Modifier.toString(field.getModifiers()) +" " +field.getType().getSimpleName() + " " +field.getName());
        }
    }
    //it will print only fields inside the class(without including inherited) declared as public
    static void fields(Class cls)
    {
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields)
        {
            System.out.println(Modifier.toString(field.getModifiers()) +" " +field.getType().getSimpleName() + " " +field.getName());
        }
    }

}
class A{
    public String name;
    public int age;
    B b; //it won't show on allFields cause private it's not declared public
}
class B extends A{
    public String surname;
    private int id;
    A a;
}
