package com.banti.reflection.slide28;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
public class ObtainFieldModifiers {
    public static void main(String[] args) {
        Field[] fields = Boolean.class.getDeclaredFields();
        for(Field field:fields)
        {
            System.out.println(Modifier.toString(field.getModifiers()) + " " + field.getType().getCanonicalName()+" "+ field.getName());
        }
    }
}
