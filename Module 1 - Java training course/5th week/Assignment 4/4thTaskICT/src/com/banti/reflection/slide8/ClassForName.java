package com.banti.reflection.slide8;

import java.util.Arrays;

public class ClassForName{
    public static void main(String[] args) {
        try {
            Class checkName = Class.forName("com.banti.reflection.slide8.A"); //also add the whole path/package
            System.out.println(checkName.getName());
            System.out.println(checkName.getClassLoader());
            System.out.println(Arrays.toString(checkName.getInterfaces()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

class A implements B,C{

}
interface B{

}
interface C{

}