package com.banti.reflection.slide10;

import java.util.Arrays;

public class GetClass {
    public static void main(String[] args) {
        A a = new A("John",20);
        print(a);

        Class booleanCls = boolean.class;
        System.out.println(booleanCls.getName());
        Class gc = GetClass.class;
        System.out.println(gc.getName());
    }
    static void print(Object obj)
    {
        Class test = obj.getClass();
        System.out.printf("Name: %s\n",test.getName());
        System.out.printf("Constructors: %s\n",Arrays.toString(test.getConstructors()));
    }
}

record A(String name, int age){ }
