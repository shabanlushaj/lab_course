package com.banti.reflection.slide27;

import java.lang.reflect.Field;
import java.util.List;

//partly from docs.oracle
public class FieldSpy<T> {
    public boolean[][] b = {{ false, false }, { true, true } };
    public String name  = "Alice";
    public List<T> list;
    public T val;

    public static void main(String... args) {
        try {
            Class<?> c = Class.forName("com.banti.reflection.slide27.FieldSpy");
            Field f = c.getField("list");
            System.out.format("Type: %s%n", f.getType().getSimpleName());
            System.out.format("GenericType: %s%n", f.getGenericType().getTypeName());

            // production code should handle these exceptions more gracefully
        } catch (ClassNotFoundException | NoSuchFieldException x) {
            x.printStackTrace();
        }
    }
}
