package com.banti.reflection.slide20;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ConstructorInstance {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Student> student = Student.class;

        Constructor<Student> constructor = student.getConstructor(String.class,int.class);
        constructor.setAccessible(true);
        Student s = (Student)constructor.newInstance("John",20);
        System.out.printf("Name: %s\nAge: %d",s.name(),s.age());


    }
}

record Student(String name,int age){}
