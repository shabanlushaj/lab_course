package com.unipi.talepis;

import java.util.ArrayList;

public class Car {
    private int maxSpeed;
    private int minSpeed;
    private int currentSpeed;
    private ArrayList<SpeedListener> speedListeners = new ArrayList<>();

    public Car(int maxSpeed, int minSpeed, int currentSpeed) {
        this.maxSpeed = maxSpeed;
        this.minSpeed = minSpeed;
        this.currentSpeed = currentSpeed;
    }
    public synchronized void addSpeedListener(SpeedListener listener){
        if (!speedListeners.contains(listener))
            speedListeners.add(listener);
    }
    public void speedUp(int increment){
        this.currentSpeed+=increment;
        if (this.currentSpeed>this.maxSpeed){
            processSpeedEvent(new SpeedEvent(maxSpeed,minSpeed,currentSpeed));
        }
    }
    public void slowDown(int decrement){
        this.currentSpeed-=decrement;
        if (this.currentSpeed<this.minSpeed){
            processSpeedEvent(new SpeedEvent(maxSpeed,minSpeed,currentSpeed));
        }
    }
    private void processSpeedEvent(SpeedEvent event){
        ArrayList<SpeedListener> tmp;
        synchronized (this){
            if (speedListeners.size()==0)
                return;
            tmp = new ArrayList<>(speedListeners);
        }
        for (SpeedListener listener :
                tmp) {
            listener.speedExceeded(event);
            listener.speedGoneBelow(event);
        }
    }
}
