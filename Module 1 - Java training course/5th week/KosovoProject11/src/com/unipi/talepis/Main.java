package com.unipi.talepis;

public class Main {

    public static void main(String[] args) {
        Car car1 = new Car(100,40,60);
        SpeedListener listener = new MySpeedListener();
        car1.addSpeedListener(listener);

        car1.speedUp(20);
        car1.speedUp(40);
        car1.slowDown(100);
        car1.slowDown(10);
        car1.speedUp(200);

    }
}
