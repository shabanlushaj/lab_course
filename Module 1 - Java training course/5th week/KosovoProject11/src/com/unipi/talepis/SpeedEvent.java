package com.unipi.talepis;

public class SpeedEvent {
    private int maxSpeed;
    private int minSpeed;
    private int currentSpeed;

    public SpeedEvent(int maxSpeed, int minSpeed, int currentSpeed) {
        this.maxSpeed = maxSpeed;
        this.minSpeed = minSpeed;
        this.currentSpeed = currentSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getMinSpeed() {
        return minSpeed;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }
}
