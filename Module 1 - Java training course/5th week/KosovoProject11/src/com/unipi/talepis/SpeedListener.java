package com.unipi.talepis;

public interface SpeedListener {
    void speedExceeded(SpeedEvent event);
    void speedGoneBelow(SpeedEvent event);
}
