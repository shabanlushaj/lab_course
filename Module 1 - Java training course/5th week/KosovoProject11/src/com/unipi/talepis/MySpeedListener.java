package com.unipi.talepis;

public class MySpeedListener implements SpeedListener{
    @Override
    public void speedExceeded(SpeedEvent event) {
        if (event.getCurrentSpeed()>event.getMaxSpeed())
            System.out.println("Be careful, you have exceeded max speed by "
            +(event.getCurrentSpeed()-event.getMaxSpeed()+"km/h"));
    }
    @Override
    public void speedGoneBelow(SpeedEvent event) {
        if (event.getCurrentSpeed()<event.getMinSpeed())
            System.out.println("You drive very slowly, please accelerate by "
                    +(event.getMinSpeed()-event.getCurrentSpeed()+"km/h"));
    }
}
