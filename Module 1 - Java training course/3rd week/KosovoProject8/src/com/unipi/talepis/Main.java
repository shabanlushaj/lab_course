package com.unipi.talepis;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        /*List<String> list = Arrays.asList("c2","a1","a2","a3","b1","b2");

        list
                .stream()
                .filter(s -> s.endsWith("2"))
                .map(String::toUpperCase) //ClassName::new //for constructor
                .sorted()
                .forEach(System.out::println);

        Arrays.asList("arbnor","efthimios")
                .stream()
                .findFirst()
                .ifPresent(System.out::println);

        Stream.of("Besmir","Ornela")
                .findAny()
                .ifPresent(System.out::println);

        IntStream.range(5,67)
                .map(n-> 2*n+3)
                .average()
                .ifPresent(System.out::println);*/
        /*Stream.of("a1","a2","a3","b1","b2","c2")
                .filter(s -> {
                    System.out.println("filter: "+ s);
                    return true;
                })
                .forEach(s -> System.out.println("forEach: "+ s));*/

        /*Stream.of("a1","a2","a3","b1","b2","c2")
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .anyMatch(s -> {
                    System.out.println("anyMatch: "+ s);
                    return s.startsWith("B");
                });*/
        /*Stream.of("a1","a2","a3","b1","b2","c2")
                .map(s -> {
                    System.out.println("map: "+s);
                    return s.toUpperCase();
                })
                .filter(s -> {
                    System.out.println("filter: "+s);
                    return s.startsWith("B");
                })
                .forEach(s -> System.out.println("forEach: "+s));*/
        /*Stream.of("a1","a2","a3","b1","b2","c2")
                .filter(s -> {
                    System.out.println("filter: "+s);
                    return s.startsWith("b");
                })
                .sorted((s1,s2)->{
                    System.out.printf("soft: %s, %s\n",s1,s2);
                    return s1.compareTo(s2);
                })
                .map(s -> {
                    System.out.println("map: "+s);
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.println("forEach: "+s));*/
        /*Stream<String> stream = Stream.of("a1","a2","a3","b1","b2","c2")
                .filter(s -> s.startsWith("b"));
        stream.anyMatch(s -> true);*/
        //stream.noneMatch(s ->true); //Exception
        //In order to use a stream with different terminal functions you may use a supplier!
        /*Supplier<Stream<String>> streamSupplier= ()->
                Stream.of("a1","a2","a3","b1","b2","c2")
                .filter(s -> s.startsWith("a"));
        streamSupplier.get().anyMatch(s->true);
        streamSupplier.get().noneMatch(s->true);*/

    }
}
