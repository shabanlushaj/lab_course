package com.unipi.talepis;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public class AdvancedStreams {
    public static void main(String... args){
        List<Student> students =
                Arrays.asList(
                        new Student("Ornela",20),
                        new Student("Arlinda", 30),
                        new Student("Mevlane",20),
                        new Student("Dielleza", 50)
                );
        /*List<Student> filtered =
                students
                .stream()
                .filter(student -> student.name.startsWith("A"))
                .collect(Collectors.toList());
        System.out.println(filtered);*/
        /*Map<Integer,List<Student>> studentsByAge =
                students.stream()
                .collect(Collectors.groupingBy(student -> student.age));
        studentsByAge
                .forEach((age,st)-> System.out.format("age %d: %s\n", age,st));*/
        /*Double averageAge = students
                .stream()
                .collect(Collectors.averagingInt(st->st.age));
        System.out.println(averageAge);*/

        /*IntSummaryStatistics stats = students
                .stream()
                .collect(Collectors.summarizingInt(st->st.age));
        System.out.println(stats);*/
        /*String str = students
                .stream()
                .collect(Collectors.joining("as","and"));*/
        /*students
                .stream()
                .reduce((s1,s2)->s1.age>s2.age?s1:s2)
                .ifPresent(System.out::println);*/
        /*Student student =
                students
                .stream()
                .reduce(new Student("",0),(s1,s2)->{
                    s1.age += s2.age;
                    s1.name += s2.name;
                    return s1;
                });
        System.out.println("name:"+student.name);
        System.out.println("age:"+student.age);*/
        ForkJoinPool pool = ForkJoinPool.commonPool();
        //System.out.println(pool.getParallelism());
        Arrays.asList("a1","a2","a3","b1","b2","c2")
                .parallelStream()
                .filter(s -> {
                    System.out.format("filter: %s [%s]\n",s,Thread.currentThread().getName());
                    return true;
                })
                .map(s -> {
                    System.out.format("map: %s [%s]\n",s,Thread.currentThread().getName());
                    return s.toUpperCase();
                })
                .sorted((s1,s2)->{
                    System.out.format("sort: %s,%s [%s]\n",s1,s2,Thread.currentThread().getName());
                    return s1.compareTo(s2);
                })
                .forEach(s -> {
                    System.out.format("forEach: %s [%s]\n",s,Thread.currentThread().getName());
                });

    }
}

class Student{
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return name;
    }
}
