package com.banti.slide29;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayIteration {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++)
        {
            list.add(i);
        }
        int num = 5;
        //iterator method
        Iterator<Integer> iterator = list.listIterator();
        while (iterator.hasNext())
        {
            list.set(iterator.next(),num++);
        }
        System.out.println(list);
        /*for (Integer i: list)
        {
            list.set(i,num++);
        }
        System.out.println(list); //index out of bonds
*/
    }
}
