package com.banti.slide20;

public class MyString2 {
    private String s;

    public MyString2(String s) {
        this.s = s;
    }

    public String getValue() {
        return this.s;
    }

    public int compare(String s) {
        final int len = this.s.length() > s.length() ? this.s.length() : s.length();

        for(int i =0;i< len; i++) {
            if (i >= s.length()) return 1;
            if (i >= this.s.length()) return -1;

            char a = this.s.charAt(i);
            char b = s.charAt(i);

            int diff = a - b;
            if (diff != 0) return diff;
        }

        return 0;
    }

    public MyString2 substring(int begin) {
        StringBuilder s1=new StringBuilder();
        for(int i=begin;i<this.s.length();i++)
            s1.append(this.s.charAt(i));
        return new MyString2(s1.toString());
    }

    public MyString2 toUpperCase() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.s.length(); i++) {
            char c = this.s.charAt(i);
            if (c >= 'a' && c <= 'z') {
                c -= 32; // a - A = 32
            }
            sb.append(c);
        }
        return new MyString2(sb.toString());
    }

    public char[] toChars() {
        char[] c = new char[this.s.length()];
        for (int i = 0; i < this.s.length(); i++) {
            c[i] = this.s.charAt(i);
        }
        return c;
    }

    public static MyString2 valueOf(boolean b) {
        return new MyString2(b ? "true" : "false");
    }

    public static void main(String[] args) {
        MyString2 myStr = new MyString2("abc");

        System.out.println(myStr.compare("Ab"));
        System.out.println(myStr.compare("ab"));
        System.out.println(myStr.compare("abcd"));
        System.out.println(myStr.compare("ha"));
        System.out.println(myStr.compare("testing"));

        System.out.println(myStr.substring(1).getValue());
        System.out.println(myStr.toUpperCase().getValue());
        System.out.println(MyString2.valueOf(true).getValue());
        System.out.println(myStr.toChars());
    }
}