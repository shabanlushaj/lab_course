package com.banti.slide20;

public class MutableString {
    public static void main(String[] args) {
        String firstName = "Shaban";
        String lastName = "Lushaj";
        System.out.println(concat(firstName,lastName));
        stringBuilderExamples();
        //palindrome - sb reverse
        System.out.println(palindrome("kimik"));
        System.out.println(palindrome("kimiku"));
    }

    static String concat(String firstName, String lastName)
    {
        StringBuilder sb = new StringBuilder(firstName);
        sb.append(" ").append(lastName);
        return sb.toString();
    }
    static void stringBuilderExamples()
    {
        //length and capacity
        StringBuilder sb = new StringBuilder(10);
        System.out.println(sb.capacity());
        sb.append("Hello World damn");
        System.out.println(sb.capacity());
        System.out.println(sb);
        sb.setLength(8);
        System.out.println(sb);

        //insert and delete
        A a = new A();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Hello");
        sb2.insert(3,a);
        System.out.println(sb2);

        sb2.delete(3,a.toString().length()+3);
        System.out.println(sb2);
    }
    static boolean palindrome(String a)
    {
        StringBuilder sb = new StringBuilder(a);
        return a.equals(sb.reverse().toString());
    }
}


class A{

}
