package com.banti.slide85;

public interface Writer {
    void doSomething();
}
class Pen {
    public static void main(String[] args) {
        Writer writer = new Writer() {
            int x = 5;
            @Override
            public void doSomething() {
                System.out.println("Pen writing " +x);
                otherFunction();
            }

            public void otherFunction() {
                System.out.println("Method inside anonymous implementation" + (x+1));
            }

        };
        writer.doSomething();
        //otherFunction & x -> cannot access outside the class
    }
}