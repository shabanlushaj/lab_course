package com.banti.slide79;

public class Casting {
    public static void main(String[] args) {
        B b = new B();
        System.out.println(b.x);
        System.out.println(b.doubleValue());
        System.out.println("################");
        A a = (B) b;
        System.out.println(a.x);
        System.out.println(a.doubleValue());

    }
}
class A{
    String var = "Hello";
    int x = 5;
    String printLine()
    {
        return var;
    }
    int doubleValue()
    {
        return x*x;
    }
}
class B extends A{
    String var = "Hello from B";
    int x = 10;
    String printLine()
    {
        return var;
    }
    int doubleValue()
    {
        return x*x;
    }
}
