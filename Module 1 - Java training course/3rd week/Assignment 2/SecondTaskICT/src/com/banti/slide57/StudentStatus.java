package com.banti.slide57;

public enum StudentStatus {
    FRESHMAN, SOPHOMORE, JUNIOR, SENIOR
}
