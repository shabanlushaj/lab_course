package com.banti.slide57;

import java.util.Date;

public class Employee extends Person {
    protected String office;
    protected double salary;
    protected Date date;

    public Employee(String name, String address,String phone,String email,
                    String office,double salary, Date date) {
        super(name,address,phone,email);
        this.office = office;
        this.salary = salary;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Employee: " + this.name;
    }
}