package com.banti.slide57;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Person p = new Person("John", "tokyo", "123", "john@mail.com");
        Student s = new Student("Jane", "berlin", "456", "jane@mail.com",
                StudentStatus.SOPHOMORE);
        Employee e = new Employee("Jack", "london", "123", "jack@mail.co.uk",
                "616", 350, new Date());
        System.out.println(p.toString());
        System.out.println(s.toString());
        System.out.println(e.toString());

    }
}
