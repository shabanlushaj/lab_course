package com.banti.slide23;

import java.util.ArrayList;
import java.util.Random;

public class FifoQueue<T>{
    private ArrayList<T> queue = new ArrayList<>();
    public void push(T elem)
    {
        this.queue.add(elem);
    }
    public T pop()
    {
        if(this.queue.size()==0)
            return null;
        else
            return this.queue.remove(0);
    }
    public void setRandom(T elem)
    {
        Random random = new Random();
        this.queue.set(random.nextInt(this.queue.size()), elem);
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        for (T t : queue) {
            sb.append(t);
            sb.append(' ');
        }
        return sb.toString();
    }
}
class Main{
    public static void main(String[] args) {
        FifoQueue<String> fifoQueue = new FifoQueue<>();
        fifoQueue.push("apple");
        fifoQueue.push("pear");
        fifoQueue.push("strawberry");
        System.out.println(fifoQueue.toString());
        fifoQueue.pop();
        System.out.println(fifoQueue.toString());

        FifoQueue<Integer> fifoInt = new FifoQueue<>();
        for(int i = 0; i<10;i++)
            fifoInt.push(i);

        System.out.println(fifoInt.toString());
        fifoInt.setRandom(10);
        System.out.println(fifoInt.toString());
    }
}
