package com.banti.slide40;

public class SwitchAndIf {
    public static void main(String[] args) {
        String cardName = "", suit = "";
        int card = (int) (Math.random() * 52) + 1; // range 1-52

        switch (card % 13) {
            case 1 -> cardName = "Ace";
            case 2 -> cardName = "2";
            case 3 -> cardName = "3";
            case 4 -> cardName = "4";
            case 5 -> cardName = "5";
            case 6 -> cardName = "6";
            case 7 -> cardName = "7";
            case 8 -> cardName = "8";
            case 9 -> cardName = "9";
            case 10 -> cardName = "10";
            case 11 -> cardName = "Jack";
            case 12 -> cardName = "Queen";
            case 0 -> cardName = "King";
        }

        if (card <= 13) {
            suit = "Clubs";
        } else if (card <= 26) {
            suit = "Diamonds";
        } else if (card <= 39) {
            suit = "Hearts";
        } else {
            suit = "Spades";
        }

        System.out.printf("The card you picked is %s of %s", cardName, suit);
    }

}
