package com.banti.slide95;

public class Pen {
    String occupation = "Police";
    void doSomething()
    {
        System.out.println("I am doing something");
    }
}
class Intro{
    public static void main(String[] args) {
        Pen pen = new Pen(){
            void job()
            {
                System.out.println("I am a "+occupation);
            }
        };
        pen.doSomething();
        //pen.job();
    }
}
