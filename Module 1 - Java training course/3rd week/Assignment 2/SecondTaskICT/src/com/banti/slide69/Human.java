package com.banti.slide69;

public class Human {
    private final int id;
    private final int age;
    private String name;
    private String occupation;

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Human(int id, int age, String name)
    {
        this.id = id;
        this.age = age;
        this.name = name;
    }


    public void job(){
        System.out.println("My job is:" + occupation);
    }

    public static void main(String[] args) {
        Human human = new Human(100,20,"John");
        human.setOccupation("Police");
        Student student = new Student(200,21,"Doe");
        human.job();
        student.job();
    }
}
