package com.banti.slide69;

public class Student extends Human{
    public Student(int id, int age, String name) {

        super(id, age, name);
    }

    @Override
    public void job()
    {
        System.out.println("My job is: STUDENT!!!");
    }

}
