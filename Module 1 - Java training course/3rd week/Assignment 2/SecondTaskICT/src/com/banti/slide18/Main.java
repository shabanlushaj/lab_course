package com.banti.slide18;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        String s1 = "Hello There";
        String s2 = new String(s1);

        System.out.println(s1==s2); // compares if the object we are referencing is the same;
        System.out.println(s1.equals(s2)); // compares the value - returns true if the values are the same
        System.out.println("########");

        String s3 = s1; // we reference the object, without creating a new one
        System.out.println(s3==s1);
        System.out.println(s3.equals(s1));
        System.out.println("########");

        String s4 = "Hello World";
        String s5 = "Hello World";
        System.out.println("Hash code: " + s5.hashCode());
        System.out.println(s4==s5);
        System.out.println(s4.equals(s5));
        s5 = "hello world";
        System.out.println("Hash code after changing value: "+s5.hashCode());
        System.out.println(s4.equals(s5));
        System.out.println(s4.equalsIgnoreCase(s5));

        System.out.println("##############################");

        //immutable, and mutable difference example
        Point point1 = new Point(1,2);
        Point point2 = point1;
        System.out.println(point1.equals(point2));
        System.out.println(point1==point2);
        point2.x = 5;
        point2.y = 6;
        System.out.println(point1); //

        System.out.println(point1.hashCode());
        System.out.println(point2.hashCode());


    }
}
