package com.unipi.talepis;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        File file = new File(args[0]);
        List<String> stringList = new ArrayList<>();
        BufferedReader reader = null;
        int lineCount = 0;
        try{
            reader = new BufferedReader(new FileReader(file));
            for(String line = reader.readLine();line!=null;line=reader.readLine()){
                stringList.add(line);
                lineCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        int howManyTimes = Integer.parseInt(args[1]);
        printRandom(stringList,howManyTimes);
        System.out.println(stringList.size());
        List<String> newList = removeDuplicates(stringList);
        System.out.println(stringList.size());
        System.out.println("##############################################");
        printRandom(newList,howManyTimes);
        System.out.println(newList.size());
    }
    public static void printRandom(List<String> strings,int count){
        Random random = new Random();
        for (int i = 0;i<=count;i++){
            System.out.println(strings.get(random.nextInt(strings.size()-1)));
        }
    }
    public static <T> List<T> removeDuplicates(List<T> list){
        Set<T> set = new LinkedHashSet<>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }
}
