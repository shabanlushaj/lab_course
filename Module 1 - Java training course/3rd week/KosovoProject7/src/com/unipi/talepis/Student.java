package com.unipi.talepis;

// Method chaining Class
public class Student {
    int age;
    String email;
    String name;
    public Student changeAge(int newAge){
        this.age = newAge;
        return this;
    }
    public Student changeEmail(String newEmail){
        this.email = newEmail;
        return this;
    }
    public Student changeName(String newName){
        this.name = newName;
        return this;
    }
    public void printStudent(){
        System.out.println("Student name is:"+name+" age:"+age+" email:"+email);
    }
    public static void main(String... args){
        new Student().changeAge(30)
                .changeEmail("mail@hotmail.com")
                .changeName("Blerta")
                .printStudent();
        Student student = new Student();
        student.changeAge(2).changeName("Arian").printStudent();
    }
}
