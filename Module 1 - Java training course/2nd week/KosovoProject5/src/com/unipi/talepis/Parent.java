package com.unipi.talepis;

public class Parent {
    public Parent() {
        System.out.println("Parent Constructor run!");
    }
    {
        System.out.println("Parent Instance Initializer");
    }
}
