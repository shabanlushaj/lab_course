package com.unipi.talepis;

public class DemoImplListener implements StateChangeListener{
    @Override
    public void onStateChange(String oldState, String newState) {
        System.out.println("Deleted "+oldState+" Just used "+newState);
    }
}
