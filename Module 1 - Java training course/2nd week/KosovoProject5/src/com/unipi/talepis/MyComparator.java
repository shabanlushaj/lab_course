package com.unipi.talepis;

public interface MyComparator {
    public boolean compare(int a, int b);
}
