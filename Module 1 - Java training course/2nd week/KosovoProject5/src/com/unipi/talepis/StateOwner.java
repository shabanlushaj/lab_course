package com.unipi.talepis;

public class StateOwner {
    public void addStateListener(StateChangeListener listener){
        listener.onStateChange("Very old","Quite new");
    }
}
