package com.unipi.talepis;

// Functional Interface!
public interface StateChangeListener {
    public void onStateChange(String oldState, String newState);
}
