package com.unipi.talepis;

public class Child extends Parent{

    int age;
    double height;
    {
       /* try {

        }catch (Exception e){

        }*/ // One more reason is Exception handling
        //for (int i =0;i<20;i++) //One reason is that you can write code
        age = 10;
        height = 1.0;
    }
    public Child(int age, double height) {
        this.age = age;
        this.height = height;
        System.out.println("Age/Height Child Constructor run!");
    }

    public Child(double height) {
        this.height = height;
    }

    public Child(int age) {
        this.age = age;
        System.out.println("Age Child Constructor run!");
    }

    public Child() {
        System.out.println("Default Child Constructor run!");
    }
    {
        System.out.println("Child Instance Initializer");
    }

}
