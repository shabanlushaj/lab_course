package com.unipi.talepis;

public class Main {
static String name = "Efthimios";

    public static void main(String[] args) {
	    // Bellow code is about instance initializers
        /*Child child1 = new Child();
        System.out.println("----------------------");
        Child child2 = new Child(24);
        System.out.println("----------------------");
        Child child3 = new Child(24, 1.80);*/

        // Bellow code is about lambda expressions
        StateOwner stateOwner = new StateOwner();
        DemoImplListener demoImplListener = new DemoImplListener();
        stateOwner.addStateListener(demoImplListener);
        stateOwner.addStateListener(new StateChangeListener() {
            int i,j;
            @Override
            public void onStateChange(String oldState, String newState) {
                System.out.println(oldState+" Hello from anonymous! "+newState);
            }
        });

        //Lambda expression
        stateOwner.addStateListener((oldState, newState)->System.out.println(name+" Hello from Lambda!!!"));

        //()-> System.out.println("hello"); //Lambda Expression with no parameters
        //()-> return true; //lambda Expression that returns value
        /*()-> {
            System.out.println("hello");
            System.out.println("world");
        }*/ // Multi line lambda expression

        MyComparator myComparator = (a, b) -> a > b;
        System.out.println(myComparator.compare(8,2));
    }


}
