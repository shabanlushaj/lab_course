package com.company.banti;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        //example java Main.java names.txt 10
        if (args.length != 2)
            throw new Error("Number of args isn't valid");

        File file = new File(args[0]);
        if (!file.exists())
            throw new FileNotFoundException();
        List<String> names = new ArrayList<>();
        Set<String> set = new HashSet<>();
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            names.add(input.next());
        }
        Random random = new Random();
        int nLines = Integer.parseInt(args[1]);
        int namesLength;
        for (int i = 0; i < nLines; i++)
        {
            namesLength = random.nextInt(names.size());
            System.out.println(names.get(namesLength));
            set.add(names.get(namesLength));
        }
        System.out.println(set);
    }
}
