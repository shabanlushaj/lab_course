package com.unipi.talepis;

public class Main {

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        // Do your work
        // or, even better, call a method to test
        long sum1 = 0;
        for (long i = 0 ; i <= Integer.MAX_VALUE; i++){
            sum1+=i;
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println(sum1);
        System.out.println(duration);

        long startTime2 = System.nanoTime();
        // Do your work
        // or, even better, call a method to test
        Long sum2 = 0L;
        for (long i = 0 ; i <= Integer.MAX_VALUE; i++){
            sum2+=i;
        }
        long endTime2 = System.nanoTime();
        long duration2 = endTime2 - startTime2;
        System.out.println(sum2);
        System.out.println(duration2);
    }
}
