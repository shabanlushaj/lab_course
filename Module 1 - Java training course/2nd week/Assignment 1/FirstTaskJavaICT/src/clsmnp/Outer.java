package clsmnp;

public class Outer {

    public static class StaticNested{
        void printStaticNested(){
            System.out.println("Static nested class");
        }
    }

    public class Nested{
        void printLocation()
        {
            System.out.println("Outer > Nested");
        }
    }

    void printLocation(){
        System.out.println("Outer class");
    }

    void anynomousEx(){


        class Local{
            void printLocation(){
                System.out.println("I am inside a function");
            }
        }
        Local local = new Local();
    }

}