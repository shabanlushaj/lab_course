package clsmnp;

public class Animal
{
    void palidhje()
    {
        System.out.println("Hello from palidhje");
    }
    static String name = "Animal type A";
    public static class Monkey{
        static void Print(){
            System.out.println(name);
        }
    }
    class Chest{
        void print()
        {
            System.out.println("Hello -> Chest");
        }
    }

    void doSomething(){
        class rand{
            public void print()
            {
                System.out.println("Hello World -> rand print");
            }
        }
        Animal animal5 = new Animal(){
            @Override
            void palidhje()
            {
                System.out.println("Anonymous > method override");
                printo();
            }
            public void printo(){
                System.out.println("Call me inside anonym. class");
            }
        };
        animal5.palidhje();
        rand rnd = new rand();
        rnd.print();
    }

}
class Intro {
    public static void main(String[] args) {
        Animal animal1 = new Animal();
        Animal.Chest animal2 = animal1.new Chest();
        animal2.print();
        animal1.doSomething();
        Animal.Monkey.Print();
    }
}
