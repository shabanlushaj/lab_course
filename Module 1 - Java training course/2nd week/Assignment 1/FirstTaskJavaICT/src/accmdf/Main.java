package accmdf;

public class Main {
    public static void main(String[] args) {
        Office offObj = new Office("45ABC");
        //offObj.hours;  //it will throw an error, because we can't access private outer class

        System.out.println(offObj.getHours()); //We should add a getter method to access private methods


        String offName = offObj.officeName; //protected variable
        System.out.println(offName);

        offObj.officeLocation(); //we can access also default method declarations within package

        Red redColor = new Red();
        redColor.ColorChoice();
        redColor.NearColor();
        redColor.colorYellow();

        boolean isInst = redColor instanceof Colors;
        if(isInst)
            System.out.println("Nice");


        Fedora fedora = new Fedora();
        Linux linux = new Linux();

        linux.shell();
        fedora.shell();

        linux.os();
        fedora.os();




    }
}
