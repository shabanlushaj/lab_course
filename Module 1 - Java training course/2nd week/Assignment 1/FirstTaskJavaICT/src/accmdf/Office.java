package accmdf;

public class Office {
    private int hours;
    private String workerId;
    protected String officeName = "403A";


    void officeLocation(){
        System.out.println("Office location is 23423 Brooklyn Street");
    }


    private Office()
    {
        super();
    }
    public Office(String workerId,int hours) {
        this.workerId = workerId;
        this.hours= hours;
    }
    public Office(String workerId)
    {
        this(workerId,6);
    }

    public int getHours() {
        return hours;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }
}
