package accmdf;

public class Linux {
     String terminal = "Bash";
     final String system = "UNIX";

    void shell(){
        terminal = "Bash2.0";
        System.out.println("My default terminal is: " + terminal);
    }

    final void os(){
        //system = "unix 14.5";
        System.out.printf("I am in %s family.\n" ,system);
    }
}

final class Fedora extends Linux{
    @Override
    void shell(){
        System.out.println("My default terminal is: GNU bash");
    }


/*It can't be overriden because it's declared as final in Linux class
    void os(){
        System.out.println("I am an unix distro");
    }*/
    /*
    @Override
    void os(){

    */}


