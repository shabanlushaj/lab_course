package methret;

public class School {
    private int schoolClass;
    private static int count;

    public School(int schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String SchoolName(String name) {
        return "Your school name is: " + name;
    }

    public void changeClass(int schoolClass) {
        count = schoolClass + 1;
        this.schoolClass = count;
    }

    public String honours(String honourName) {
        String teacher1 = bestTeacher();
        return honourName + "is given to: " + teacher1;
    }

    public static String bestTeacher() {
        return "Efthimios Alepis";
    }

    public int getSchoolClass() {
        return schoolClass;
    }

    public static int chooseMaxValue(int... args) {
        int max = args[0];
        for (int i = 1; i < args.length; i++) {
            if (args[i] > max)
                max = args[i];
        }
        return max;
    }
}