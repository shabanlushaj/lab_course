package com.company.banti;

public class Modern {
    //this is a class
    public static void Run(){
        System.out.println("Static import");
    }
    public void Time(){
        System.out.println("Time");
    }
    static String name = "Shaban";
    public void printName()
    {
        System.out.println(name);
    }

    /*String surname = "Lushaj";
    public static void printSurname(){
        System.out.println(surname);
    }*/
    //static methods can't take access non-static vars
}
