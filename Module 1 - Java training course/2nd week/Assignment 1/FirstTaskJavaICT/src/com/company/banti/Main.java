package com.company.banti;

public class Main {

    public static void main(String[] args) {
        Modern modernObj = new Modern();//this is an object

        char mojito = 'A';
        //char vesper = "A"; //It will throw an error because dtype is 'String'
        System.out.println(mojito);
        System.out.println("Character is letter: "+ Character.isLetter(mojito));
        //System.out.println("Character is letter: "+ Character.isLetter("A")); // It will throw an error

        //Autoboxing - converting primitive to obj wrapper class
        int pNo = 5;
        Integer objNo = pNo; //Integer.valueOf(n1)
        //System.out.println(pNo.hashCode()); //it won't compile cuz it's primitive data type
        System.out.println(objNo.hashCode());

        //Unboxing - obj wrapper class to primitive
        Integer objNo2 = 15;
        int pNo2 = objNo2; //Java compiler will convert : num1.intValue();




    }
}
