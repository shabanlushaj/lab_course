package DBConfig;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class NavData extends JFrame {
    private String name;
    private java.sql.Date birthdate;
    private int id;
    private String city;
    private java.sql.Date timestamp;
    private boolean disease;

    public String getName() {
        return name;
    }

    public java.sql.Date getBirthdate() {
        return birthdate;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean isDisease() {
        return disease;
    }

    public NavData(int id){
        String query = "Select * from Patients2 where id=?";
        try(Connection connection = UserLogin.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                this.name = resultSet.getString("fullname");
                this.id = resultSet.getInt("id");
                this.birthdate = resultSet.getDate("birthdate");
                this.timestamp = resultSet.getDate("time_stamp");
                this.city = resultSet.getString("city");
                this.disease = resultSet.getBoolean("disease");
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public static ArrayList<Integer> getInfoId() throws SQLException {
        ArrayList<Integer> idInfo = new ArrayList<>();
        String query = "select * from Patients2 order by time_stamp limit 4";
        try(Connection connection = UserLogin.connect();
            PreparedStatement statement = connection.prepareStatement(query)
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                idInfo.add(resultSet.getInt(1));
            }
        }
        return idInfo;
    }
    public static void fillNav(ArrayList<Integer> arrayList,int element,JLabel id,JLabel name, JLabel birthday, JLabel time){
        NavData data = new NavData(arrayList.get(element));
        id.setText(String.valueOf(data.getId()));
        name.setText(data.getName());
        birthday.setText(String.valueOf(data.getBirthdate()));
        time.setText(String.valueOf(data.getTimestamp()));
    }
}
