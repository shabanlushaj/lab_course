package DBConfig;

import java.sql.*;

public class UserLogin {
     public boolean login(String name, String password){
        String loginSQL = "SELECT id " +
                "FROM users " +
                "WHERE username=? " +
                "AND password=?";
        try(Connection connection = connect();
            PreparedStatement statement = connection.prepareStatement(loginSQL)){
            statement.setString(1,name);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
         return false;
     }

    public static Connection connect(){
        DBCreds credentials = new DBCreds();
        String url = "jdbc:mysql://"+credentials.host+":"+credentials.port+"/"+credentials.dbname;
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url,credentials.username,credentials.password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
