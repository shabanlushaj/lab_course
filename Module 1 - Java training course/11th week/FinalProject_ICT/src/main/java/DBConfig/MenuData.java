package DBConfig;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MenuData {
    private PatientDb patientDb = new PatientDb();

    public String retrieveInfo(int id,String column)
    {
        String title = "";
        String query = "select * from menu where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                title =  resultSet.getString(column);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return title;
    }
    public void readFromFile(String path, JTextArea text)
    {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String s;
            StringBuilder builder = new StringBuilder();
            while ((s=reader.readLine())!=null){
                builder.append(s).append("\n");
            }
            text.setText(builder.toString());
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }
    static public int numOfData(String query)
    {
        int num = 0;
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                num = resultSet.getInt(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return num;
    }

    public static HashMap<String, Integer> cityInfo(String query)
    {
        HashMap<String, Integer> info = new HashMap<>();
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
             ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next())
            {
                info.put(resultSet.getString(1),resultSet.getInt(2));
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return info;
    }

    public static void fillData(HashMap<String, Integer> hmap,JLabel k1, JLabel k2, JLabel k3, JLabel v1, JLabel v2, JLabel v3)
    {
        String[] cityValues = new String[hmap.size()];
        Integer[] cityCount = new Integer[hmap.size()];
        hmap.keySet().toArray(cityValues);
        hmap.values().toArray(cityCount);

        if (cityValues.length == 1) {
            k1.setText(cityValues[0]);
            v1.setText(String.valueOf(cityCount[0]));
        }
        else if (cityValues.length==2) {
            k1.setText(cityValues[0]);
            v1.setText(String.valueOf(cityCount[0]));
            k2.setText(cityValues[1]);
            v2.setText(String.valueOf(cityCount[1]));
        }
        else{
            k1.setText(cityValues[0]);
            v1.setText(String.valueOf(cityCount[0]));

            k2.setText(cityValues[1]);
            v2.setText(String.valueOf(cityCount[1]));

            k3.setText(cityValues[2]);
            v3.setText(String.valueOf(cityCount[2]));

        }
    }
}
