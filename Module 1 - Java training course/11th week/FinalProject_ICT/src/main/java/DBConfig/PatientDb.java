package DBConfig;

import java.sql.*;
import java.time.LocalDate;
import java.time.Period;

public class PatientDb {
    private  String name;
    private  java.sql.Date birthday;
    private  String city;
    private java.sql.Date timestamp;
    private  boolean disease;

    public String getName() {
        return name;
    }
    public int getAvg()
    {
        LocalDate today = LocalDate.now();                          //Today's date
        LocalDate birthday = (this.birthday.toLocalDate());
        Period p = Period.between(birthday,today);
        return p.getYears();
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getCity() {
        return city;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean isDisease() {
        return disease;
    }

    public PatientDb(String name, java.sql.Date birthdate, String city, java.sql.Date timestamp, boolean disease) {
        this.name = name;
        this.birthday = birthdate;
        this.city = city;
        this.timestamp = timestamp;
        this.disease = disease;
    }
    public PatientDb(){}

    public boolean insert() {
        String query = "insert into Patients2 (fullname,birthdate,time_stamp,city,disease) values (?, ?, ?, ?, ?)";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, this.name);
            preparedStatement.setDate(2, this.birthday);
            preparedStatement.setDate(3, this.timestamp);
            preparedStatement.setString(4, this.city);
            preparedStatement.setBoolean(5, this.disease);

            int rs = preparedStatement.executeUpdate();
            return rs > 0;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean update(int id, String fullname, Date birthday, Date time_stamp, String city, boolean disease) {
        String query = "update Patients2 set fullname = ?, birthdate = ?, time_stamp = ?, city = ?, disease = ? where id = ?";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, fullname);
            preparedStatement.setDate(2, birthday);
            preparedStatement.setDate(3, time_stamp);
            preparedStatement.setString(4, city);
            preparedStatement.setBoolean(5, disease);
            preparedStatement.setInt(6, id);

            int rs = preparedStatement.executeUpdate();
            return rs > 0;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public String getName(int id) {
        String name = "";
        String query = "select fullname from Patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                name =  resultSet.getString("fullname");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return name;
    }

    public String getBirthdate(int id) {
        java.sql.Date birthday = null;
        String query = "select birthdate from Patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                birthday =  resultSet.getDate("birthdate");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return birthday.toString();
    }
    public String getCity(int id) {
        String city = "";
        String query = "select city from patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                city =  resultSet.getString("city");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return city;
    }
    public String getTimestamp(int id) {
        java.sql.Date timestamp = null;
        String query = "select time_stamp from Patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                timestamp =  resultSet.getDate("time_stamp");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return timestamp.toString();
    }

    public boolean getDisease(int id) {
        boolean disease = false;
        String query = "select disease from Patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                disease =  resultSet.getBoolean("disease");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return disease;
    }

    public boolean check(int id)
    {
        String query = "select * from Patients2 where id = ? ";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
    public int count() {
        int num = 0;
        String query = "select count(*) from Patients2";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                num = resultSet.getInt(1);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return num;
    }
}
