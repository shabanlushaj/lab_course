Focus resources on non-communicable, chronic diseases such as diabetes and cardiovascular conditions

    Health issues related to lifestyle changes and an aging 
    population made this type of healthcare burden more relevant
    for healthcare systems than pandemics.  
    People aged over 65 will represent more than 11.8% of the total
    population by 2023, peaking at 29%in Japan and 22% in Western Europe2.
    