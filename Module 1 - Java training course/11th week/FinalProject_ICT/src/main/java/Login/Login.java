package Login;
import Container.MainStage;

import DBConfig.UserLogin;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class Login extends JFrame{
    private JPanel panel1;
    private JPanel login;
    private JLabel mainIcon;
    private JLabel userLabel;
    private JTextField textField1;
    private JButton submitButton;
    private JPasswordField passwordField1;
    private JLabel pwlbl;
    private JLabel usrnlbl;
    final UserLogin user = new UserLogin();

    public Login()
    {
        setTitle("Login Form");
        setPreferredSize(new Dimension(540,630));
        setContentPane(login);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        loadImage("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\images\\ict.jpg",mainIcon,250,175);
        loadImage("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\images\\user.png",usrnlbl,25,25);
        loadImage("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\images\\password.png",pwlbl,25,23);

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new Thread(()->{
                    if (user.login(textField1.getText(),new String(passwordField1.getPassword()))) {
                        try {
                            new MainStage();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        setVisible(false);
                    }
                    else
                        JOptionPane.showMessageDialog(null,"Wrong username or password");
                    }
                ).start();
            }
        });
    }
    public static void loadImage(String path, JLabel label,int width,int height){
        label.setIcon(null);
        try {
            BufferedImage img = ImageIO.read(new File(path));
            Image resized = img.getScaledInstance(width,height,
                    Image.SCALE_REPLICATE);
            label.setIcon(new ImageIcon(resized));
            label.revalidate();
            label.repaint();
            label.update(label.getGraphics());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
