package Stats;

import java.util.regex.Pattern;

class PatternMatch {

    private static Pattern DATE_PATTERN = Pattern.compile(
            "^\\d{4}-\\d{2}-\\d{2}$");
    private static Pattern NAME_PATTERN = Pattern.compile(
            "^[ A-Za-z]+$"
    );

    public static boolean matches(String date) {
        return DATE_PATTERN.matcher(date).matches();
    }
    public static boolean nameMatches(String name)
    {
        return NAME_PATTERN.matcher(name).matches();
    }

}
