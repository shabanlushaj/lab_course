package Stats;
import DBConfig.UserLogin;
import Container.SearchResults;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Stats extends JFrame{
    private JTextField textField1;
    private JButton searchButton;
    private JPanel panel1;
    private JPanel namePanel;
    private JPanel birthdayPanel;
    private JPanel timePanel;
    private JPanel idPanel;
    private JPanel panel6;

    public Stats()
    {
        namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.Y_AXIS));
        birthdayPanel.setLayout(new BoxLayout(birthdayPanel, BoxLayout.Y_AXIS));
        timePanel.setLayout(new BoxLayout(timePanel, BoxLayout.Y_AXIS));
        idPanel.setLayout(new BoxLayout(idPanel,BoxLayout.Y_AXIS));

        setContentPane(panel1);
        setTitle("VIEW STATS");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(700,500));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new Thread(()->{
                    namePanel.removeAll();
                    birthdayPanel.removeAll();
                    timePanel.removeAll();
                    idPanel.removeAll();

                    namePanel.validate();
                    birthdayPanel.validate();
                    timePanel.validate();
                    idPanel.validate();

                    namePanel.repaint();
                    birthdayPanel.repaint();
                    timePanel.repaint();
                    idPanel.repaint();

                    if(textField1.getText().equals(""))
                        getRecords(namePanel,birthdayPanel,timePanel,idPanel);
                    else if(PatternMatch.matches(textField1.getText()))
                        getRecords(textField1.getText(),namePanel,birthdayPanel,timePanel,idPanel);
                    else if(PatternMatch.nameMatches(textField1.getText()))
                        getRecordsName(textField1.getText(),namePanel,birthdayPanel,timePanel,idPanel);
                    else
                        JOptionPane.showMessageDialog(null,"Provide valid values");
                    namePanel.validate();
                    birthdayPanel.validate();
                    timePanel.validate();
                    idPanel.validate();
                }).start();

            }
        });
    }

    static void getRecords(JPanel panel1,JPanel panel2, JPanel panel3, JPanel panel4) {
        String query = "select * from patients2;";
        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                createPanels(panel1,panel2,panel3,panel4,resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    static void getRecords(String date, JPanel panel1,JPanel panel2, JPanel panel3,JPanel panel4) {
        String query = "select * from patients2 where time_stamp = ?;";
        java.sql.Date date1 =java.sql.Date.valueOf(date);

        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setDate(1,date1);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createPanels(panel1,panel2,panel3,panel4,resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    static void getRecordsName(String name, JPanel panel1,JPanel panel2, JPanel panel3, JPanel panel4) {
        String query = "select * from patients2 where fullname like ?";

        try (Connection connection = UserLogin.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1,"%"+ name+"%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                createPanels(panel1,panel2,panel3,panel4,resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    static void createPanels(JPanel panel1, JPanel panel2, JPanel panel3, JPanel panel4, ResultSet resultSet)
    {
        try {
            JLabel idLabel = new JLabel();
            JLabel nameLabel = new JLabel();
            JLabel birthLabel = new JLabel();
            JLabel timeLabel = new JLabel();
            nameLabel.setText(resultSet.getString("fullname"));
            nameLabel.setFont(nameLabel.getFont().deriveFont(20.0f));
            nameLabel.setForeground(Color.black);
            panel1.add(nameLabel);

            birthLabel.setText(resultSet.getDate("birthdate").toString());
            birthLabel.setFont(birthLabel.getFont().deriveFont(20.0f));
            birthLabel.setForeground(Color.black);
            panel2.add(birthLabel);

            timeLabel.setText(resultSet.getDate("time_stamp").toString());
            timeLabel.setFont(timeLabel.getFont().deriveFont(20.0f));
            timeLabel.setForeground(Color.black);
            panel3.add(timeLabel);

            idLabel.setText(String.valueOf(resultSet.getInt("id")));
            idLabel.setFont(idLabel.getFont().deriveFont(20.0f));
            idLabel.setForeground(Color.black);
            idLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    new SearchResults(Integer.parseInt(idLabel.getText()));
                }
            });
            panel4.add(idLabel);
        }
        catch (SQLException throwables) {
        throwables.printStackTrace();
    }
    }
}
