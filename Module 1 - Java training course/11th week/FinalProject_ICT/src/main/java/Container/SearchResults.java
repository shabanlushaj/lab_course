package Container;

import DBConfig.PatientDb;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchResults extends JFrame{
    private JLabel getName;
    private JLabel getBirthday;
    private JLabel getCity;
    private JLabel getTime;
    private JButton EDITButton;
    private JPanel panel1;
    private JCheckBox diseaseCheckBox;
    private PatientDb pt = new PatientDb();

    public static void main(String[] args) {
        new SearchResults(1);
    }
    public SearchResults(int id)
    {
        getName.setText(pt.getName(id));
        getBirthday.setText(pt.getBirthdate(id).toString());
        getCity.setText(pt.getCity(id));
        getTime.setText(pt.getTimestamp(id).toString());
        diseaseCheckBox.setSelected(pt.getDisease(id));

        setTitle("View patient");
        setPreferredSize(new Dimension(400,500));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        EDITButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords(id)).start();
                setVisible(false);
            }
        });
    }
}
