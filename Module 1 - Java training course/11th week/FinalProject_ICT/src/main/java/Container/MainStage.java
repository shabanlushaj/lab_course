package Container;

import DBConfig.MenuData;
import DBConfig.PatientDb;
import DBConfig.NavData;
import Login.Login;
import Stats.Stats;
import com.mysql.cj.log.Log;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class MainStage extends JFrame {

    private JPanel panel1;
    private JPanel addPatient;
    private JTextField textTextField;
    private JButton SEARCHButton;
    private JButton TOTALCASESButton;
    private JButton logOutButton;
    private JPanel navPanel;
    private JPanel categoriesPanel;
    private JPanel imagePanel;
    private JPanel nav1;
    private JPanel nav2;
    private JPanel nav3;
    private JPanel nav4;
    private JPanel navCreate;
    private JButton UPDATEButton;
    private JButton UPDATEButton1;
    private JButton UPDATEButton2;
    private JButton UPDATEButton3;
    private JLabel nameLabel;
    private JLabel birthLabel;
    private JLabel nav1Name;
    private JLabel nav1id;
    private JLabel nav1birthday;
    private JLabel nav1time;
    private JLabel timeLabel;
    private JLabel idLabel;
    private JButton button1;
    private JLabel ictLabel;
    private JButton SYMPTOMSButton;
    private JButton PRESCRIPTIONSButton;
    private JButton HEALTHCAREButton;
    private JButton ADVICEButton;
    private JLabel imageLabel;
    private JLabel idLabel2;
    private JLabel nameLabel2;
    private JLabel birthLabel2;
    private JLabel timeLabel2;
    private JLabel idLabel3;
    private JLabel nameLabel3;
    private JLabel birthLabel3;
    private JLabel timeLabel3;
    private JLabel idLabel4;
    private JLabel nameLabel4;
    private JLabel birthLabel4;
    private JLabel timeLabel4;
    private JLabel helloLabel;
    private JLabel numOfCases;
    private JPanel imgPanel;
    private JButton DAILYButton;
    private JButton WEEKLYButton;
    private JButton MONTHLYButton;
    private JLabel dailyCases;
    private JLabel dailyKey1;
    private JLabel dailyValue1;
    private JLabel dailyKey2;
    private JLabel dailyValue2;
    private JLabel dailyKey3;
    private JLabel dailyValue3;
    private JLabel wKey1;
    private JLabel wKey2;
    private JLabel wKey3;
    private JLabel wTotal;
    private JLabel wVal1;
    private JLabel wVal2;
    private JLabel wVal3;
    private JLabel mTotal;
    private JLabel mKey1;
    private JLabel mKey2;
    private JLabel mKey3;
    private JLabel mVal1;
    private JLabel mVal2;
    private JLabel mVal3;
    private JLabel titlePanel;
    private JLabel description;
    private JTextArea textArea1;
    private PatientDb patient = new PatientDb();

    public MainStage() throws SQLException {
        navPanel.setPreferredSize(new Dimension(getWidth(), 180));
        setTitle("Main Form");
        setPreferredSize(new Dimension(1260, 700));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        imgPanel.setPreferredSize(new Dimension(600,250));
        Login.loadImage("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\images\\ict.jpg", ictLabel, 100, 60);

        //NavData 1
        ArrayList<Integer> arrayList = NavData.getInfoId();
        NavData.fillNav(arrayList,0,idLabel,nameLabel, birthLabel, timeLabel);
        NavData.fillNav(arrayList,1,idLabel2,nameLabel2, birthLabel2, timeLabel2);
        NavData.fillNav(arrayList,2,idLabel3,nameLabel3, birthLabel3, timeLabel3);
        NavData.fillNav(arrayList,3,idLabel4,nameLabel4, birthLabel4, timeLabel4);
        //NavData --end

        //MAIN DATA - CASES
        String dailyQuery1 = "select count(*) from Patients2 where time_stamp = curdate() ";
        dailyCases.setText(String.valueOf(MenuData.numOfData(dailyQuery1)));
        String dailyQuery2 = "select City, count(*) as total from patients2 where time_stamp = curdate() group by City order by total desc";
        HashMap<String, Integer> hmap = MenuData.cityInfo(dailyQuery2);
        MenuData.fillData(hmap,dailyKey1, dailyKey2, dailyKey3, dailyValue1, dailyValue2, dailyValue3);

        String weeklyQuery1 = "select count(*) from Patients2 where time_stamp <= curdate() and time_stamp > curdate()-7";
        wTotal.setText(String.valueOf(MenuData.numOfData(weeklyQuery1)));
        String weeklyQuery2 = "select City, count(*) as total from patients2 where time_stamp <= curdate() and time_stamp > curdate()-7 group by City order by total desc";
        HashMap<String, Integer> hmap1 = MenuData.cityInfo(weeklyQuery2);
        MenuData.fillData(hmap1,wKey1,wKey2,wKey3,wVal1,wVal2,wVal3);

        String monthQuery = "select count(*) from Patients2 where time_stamp <= curdate() and time_stamp > curdate()-31";
        mTotal.setText(String.valueOf(MenuData.numOfData(monthQuery)));
        String monthQuery2 = "select City, count(*) as total from patients2 where time_stamp <= curdate() and time_stamp > curdate()-31 group by City order by total desc";
        HashMap<String, Integer> hmap2 = MenuData.cityInfo(monthQuery2);
        MenuData.fillData(hmap2,mKey1,mKey2,mKey3,mVal1,mVal2,mVal3);
        //MAIN DATA CASES

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords()).start();
            }
        });
        UPDATEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords(Integer.parseInt(idLabel.getText()))).start();
            }
        });
        UPDATEButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords(Integer.parseInt(idLabel2.getText()))).start();
            }
        });
        UPDATEButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords(Integer.parseInt(idLabel3.getText()))).start();
            }
        });
        UPDATEButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> new InsertRecords(Integer.parseInt(idLabel4.getText()))).start();
            }
        });
        SEARCHButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean numeric = textTextField.getText().matches("-?\\d+(\\.\\d+)?");
                if (numeric) {
                    if (!textTextField.getText().equals("")) {
                        int queryId = Integer.parseInt(textTextField.getText());
                        if (patient.check(queryId))
                            new Thread(() -> new SearchResults(queryId)).start();
                        else
                            JOptionPane.showMessageDialog(null, "Record doesn't exist");
                    }
                }
            }
        });
        TOTALCASESButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> numOfCases.setText(String.valueOf(patient.count()))).start();
            }
        });
        SYMPTOMSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{
                    new Stats();
                }).start();
            }
        });
        PRESCRIPTIONSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                titlePanel.setText(new MenuData().retrieveInfo(2,"name"));
                new MenuData().readFromFile("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\others\\prescription.txt",textArea1);
            }
        });
        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(() -> {
                    new Login();
                    setVisible(false);
                }).start();
            }
        });
        HEALTHCAREButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{
                    titlePanel.setText(new MenuData().retrieveInfo(3,"name"));
                    new MenuData().readFromFile("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\others\\healthcare.txt",textArea1);
                }).start();
            }
        });
        ADVICEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{
                    titlePanel.setText(new MenuData().retrieveInfo(4,"name"));
                    new MenuData().readFromFile("C:\\Users\\shaba\\IdeaProjects\\FinalProject_ICT\\src\\main\\java\\static\\others\\advice.txt",textArea1);
                }).start();
            }
        });
    }
}
