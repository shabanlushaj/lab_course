package Container;
import DBConfig.PatientDb;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class InsertRecords extends JFrame{
    private JPanel panel1;
    private JTextField textField1;
    private JCheckBox checkBox1;
    private JTextField textField3;
    private JButton INSERTButton;
    private JTextField textField2;
    private JTextField textField4;
    private Date date;
    public InsertRecords(int id)
    {
        PatientDb update = new PatientDb();
        INSERTButton.setText("UPDATE");
        setTitle("Update patient");
        setPreferredSize(new Dimension(400,500));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        textField1.setText(update.getName(id));
        textField2.setText(update.getBirthdate(id));
        textField3.setText(update.getCity(id));
        textField4.setText(update.getTimestamp(id));
        checkBox1.setSelected(update.getDisease(id));
        INSERTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                new Thread(()->{
                    if(textField1.getText().equals("") || textField3.getText().equals("") || textField2.getText().equals("") || textField4.getText().equals(""))
                        JOptionPane.showMessageDialog(null,"U should provide values inside the fields");
                    else {
                        if (update.update(id, textField1.getText(), java.sql.Date.valueOf(textField2.getText()), java.sql.Date.valueOf(textField4.getText()), textField3.getText(), checkBox1.isSelected())) {
                            JOptionPane.showMessageDialog(null, "Patient was updated successfully");
                            revalidate();
                            repaint();
                        } else
                            JOptionPane.showMessageDialog(null, "Update failed");
                    }
                }).start();
            }
        });

    }

    public InsertRecords()
    {
        setTitle("Add patient");
        setPreferredSize(new Dimension(400,500));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        INSERTButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{if (textField1.getText().equals("") || textField3.getText().equals("") || textField2.getText().equals("") || textField4.getText().equals(""))
                    JOptionPane.showMessageDialog(null, "U should provide values inside the fields");
                else {
                    PatientDb patient = new PatientDb(textField1.getText(), java.sql.Date.valueOf(textField2.getText()), textField3.getText(), java.sql.Date.valueOf(textField4.getText()), checkBox1.isSelected());

                    if (patient.insert()) {
                        JOptionPane.showMessageDialog(null, "Patient was registered successfully");
                        revalidate();
                        repaint();
                    } else
                        JOptionPane.showMessageDialog(null, "Registration failed");
                }
                }).start();
            }
        });
    }
}
