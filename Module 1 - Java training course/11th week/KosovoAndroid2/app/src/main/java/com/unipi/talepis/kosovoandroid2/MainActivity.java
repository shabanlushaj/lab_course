package com.unipi.talepis.kosovoandroid2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    SharedPreferences preferences;
    MyTts myTts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editTextTextPersonName);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        myTts = new MyTts(this);
    }
    public void method1(View view){
        Intent intent = new Intent(this,MainActivity2.class);
        intent.putExtra("myKey","Hello "+editText.getText().toString());
        startActivity(intent);
    }
    public void method2(View view){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("sharedKey1",editText.getText().toString());
        editor.apply();
    }
    public void method4(View view){
        myTts.speak(editText.getText().toString());
    }
}