package com.unipi.talepis.kosovoandroid2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    TextView textView;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textView = findViewById(R.id.textView);
        String s = getIntent().getStringExtra("myKey");
        textView.setText(s);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }
    public void method3(View view){
        String s = preferences.getString("sharedKey1","Default value for string!");
        textView.setText(s);
    }
}