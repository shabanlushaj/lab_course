package com.unipi.talepis.kosovoproject31;

import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DBController {
    private static  String url = "jdbc:sqlite:E:\\Dropbox\\java\\intellijprojects\\kosovoproject31\\src\\main\\resources\\DB\\MYSQLITEDB.db";

    @RequestMapping("/allmodels")
    public String selectModels(){
        StringBuilder builder = new StringBuilder();
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select Model from Smartphone");

            while (resultSet.next()){
                builder.append(resultSet.getString("Model")).append("<br/>");
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return builder.toString();
    }
    @RequestMapping("/fetchsmartphones")
    public List<Smartphone> fetchSmartphones(){
        List<Smartphone> smartphones = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from Smartphone");

            while (resultSet.next()){
                Smartphone smartphone = new Smartphone();
                smartphone.setId(resultSet.getString("ID"));
                smartphone.setModel(resultSet.getString("Model"));
                smartphone.setDescription(resultSet.getString("Description"));
                smartphone.setPrice(resultSet.getInt("Price"));
                smartphones.add(smartphone);
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return smartphones;
    }
    @GetMapping("/smartphone/{id}")
    public Smartphone show(@PathVariable String id) {
        Smartphone smartphone = null;
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement =
                    connection.prepareStatement("Select * From Smartphone Where id=?");
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                smartphone = new Smartphone();
                smartphone.setModel(resultSet.getString("Model"));
                smartphone.setDescription(resultSet.getString("Description"));
                smartphone.setPrice(resultSet.getInt("Price"));
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return smartphone;
    }
    @DeleteMapping("/smartphone/{id}")
    public Map<String,String> delete(@PathVariable String id){
        String message = null;
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("Delete From Smartphone Where ID=?");
            statement.setString(1,id);
            if (statement.executeUpdate()>0){
                message = "Smartphone deleted...";
            }else {
                message = "Not valid id";
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String finalMessage = message;
        return new HashMap<>(){{put("message", finalMessage);}};
    }
    @PostMapping("/newsmartphone")
    public Map<String,String> insert(@RequestBody Map<String,String> body){
        String id = body.get("id");
        String model = body.get("model");
        String description = body.get("description");
        int price = Integer.parseInt(body.get("price"));
        try{
            Connection conn = DriverManager.getConnection(url);
            PreparedStatement statement = conn.prepareStatement("Insert into Smartphone (ID, Model, Description, Price) Values (?,?,?,?)");
            statement.setString(1,id);
            statement.setString(2,model);
            statement.setString(3,description);
            statement.setInt(4,price);
            statement.execute();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return new HashMap<>(){{put("message",throwables.getLocalizedMessage());}};
        }
        return new HashMap<>(){{put("message","New Smartphone inserted succesfully!");}};
    }
    @PutMapping("/updatesmartphone/{id}")
    public Map<String,String> update(@PathVariable String id, @RequestBody Map<String,String> body){
        String model = body.get("model");
        String description = body.get("description");
        int price = Integer.parseInt(body.get("price"));
        try{
            Connection conn = DriverManager.getConnection(url);
            PreparedStatement statement = conn.prepareStatement("UPDATE Smartphone SET Model=?, Description=?, Price=? WHERE Id=?");
            statement.setString(1,model);
            statement.setString(2,description);
            statement.setInt(3,price);
            statement.setString(4,id);
            statement.execute();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return new HashMap<>(){{put("message",throwables.getLocalizedMessage());}};
        }
        return new HashMap<>(){{put("message","Smartphone updated succesfully!");}};
    }
    @PutMapping("/updatesmartphonev2/{id}")
    public Map<String,String> updateV2(@PathVariable String id, @RequestBody Map<String,String> body){
        String model = body.get("model");
        String description = body.get("description");

        try{
            Connection conn = DriverManager.getConnection(url);
            PreparedStatement statement = conn.prepareStatement("UPDATE Smartphone " +
                    "SET Model=ifnull(?,Model), " +
                    "Description=ifnull(?,Description), " +
                    "Price=ifnull(?,Price) " +
                    "WHERE Id=?");
            statement.setString(1,model);
            statement.setString(2,description);
            if (body.get("price")!=null){
                Integer price = Integer.parseInt(body.get("price"));
                statement.setInt(3,price);
            }
            statement.setString(4,id);
            statement.execute();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return new HashMap<>(){{put("message",throwables.getLocalizedMessage());}};
        }
        return new HashMap<>(){{put("message","Smartphone updated succesfully!");}};
    }
}
