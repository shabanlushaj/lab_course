package com.unipi.talepis.kosovoproject31;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Kosovoproject31Application {

    public static void main(String[] args) {
        SpringApplication.run(Kosovoproject31Application.class, args);
    }
    @GetMapping("/hello")
    public String helloWorld(){
        return "Hello World!";
    }
    @GetMapping("/givename")
    public String helloName(@RequestParam(value = "yourName", defaultValue = "Efthimios") String name){
        return "Hi "+name+"! Nice to meet you!";
    }

}
