package com.unipi.talepis;

public class Outer {

    Outer outer1 = new Outer();


    public static class StaticNested{

    }

    public class Nested{

    }
    void anotherMethod(){
        System.out.println("I am in Outer");
    }
    void doSomething(){
        Outer outer2 = new Outer(){
            @Override
            void anotherMethod() {
                System.out.println("Inside anonymous");
                doSomethingElse();
            }
            public void doSomethingElse(){
                System.out.println("Hello 2");
            }
        };

        new Outer(){
            void hello(){
                System.out.println("Hello");
            }
        }.hello();
        outer2.anotherMethod();
        class Local{

        }
        Local local = new Local();
    }

}
