package com.unipi.talepis;

public class Vehicle {
    protected String licencePlate;

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }
}

class Car extends Vehicle implements IDrive{
    private int numberOfSeats = 4;
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    @Override
    public void driving() {
        System.out.println("Yes I am driving");
    }
}

class MotorBike extends Vehicle{
    private int numberOfSeats = 2;

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
}

/*class Student{
    public class box{

    }
}*/
