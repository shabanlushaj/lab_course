package com.unipi.talepis;

import java.util.ArrayList;
import java.util.List;

public class Main {
	List<IDrive> driveList = new ArrayList<>();

    public static void main(String[] args) throws Exception {
	    /*Outer.StaticNested instance1 = new Outer.StaticNested();
	    Outer outer1 = new Outer();
	    Outer.Nested instance2 = outer1.new Nested();

	    MyInterface myInterface1 = new MyInterface() {
			@Override
			public void doIt() {

			}
		};
		//MyAbstractClass abstractClass = new MyAbstractClass(); --compile error
		MyAbstractClass abstractClass2 = new MyAbstractClass() {
			@Override
			public void abstractMethod() {

			}
		};*/
		/*Car car1 = new Car();
		Vehicle vehicle1 = car1;
		Vehicle vehicle2 = new Vehicle();
		vehicle2 = new MotorBike();
		//Car car2 = (Car)vehicle2; // be carefull when you cast
		Vehicle vehicle = car1; // upcasting
		MotorBike bike = (MotorBike)vehicle2; // downcasting
		Car car3 = new Car();
		Vehicle vehicle3 = new Vehicle();

		boolean isCar = car3 instanceof Vehicle;
		System.out.println(isCar);
		System.out.println(car3 instanceof IDrive);
		System.out.println(bike instanceof IDrive);
		System.out.println(bike instanceof Vehicle);*/
		MyClass myClass1 = new MyClass("ksv123");
		MyClass myClass2 = new MyClass(null);
        System.out.println(myClass1.hashCode());
        System.out.println(myClass1.getID());
        System.out.println(myClass2.hashCode());
        System.out.println(myClass2.getID());
        MyClass myClass3 = new MyClass("");
        System.out.println(myClass3.hashCode());
        System.out.println(myClass3.getID());
    }
}
