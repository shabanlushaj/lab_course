package com.unipi.talepis;

public class Parent {
    private int priv_Num = 234;
    /*public Parent(String name) {
    }*/

    protected int getPriv_Num() {
        return priv_Num;
    }

    public Parent() {
        System.out.println("Hello from Super!");
    }
}
