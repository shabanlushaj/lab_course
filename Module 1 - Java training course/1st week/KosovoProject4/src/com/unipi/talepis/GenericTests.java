package com.unipi.talepis;

import java.util.ArrayList;

public class GenericTests {
    public static void printArrayV1(int[] array){
        for (int i=0;i<array.length;i++)
            System.out.println(array[i]);
    }
    public static void printArrayV2(String[] array){
        for (int i=0;i< array.length;i++){
            System.out.println(array[i]);
        }
    }
    public static void printArrayV3(double[] array){
        for (int i=0;i<array.length;i++)
            System.out.println(array[i]);
    }
    public static <T> void printArrayV4(T[] array){
        for (int i=0;i<array.length;i++){
            System.out.println(array[i]);
        }
    }
    // Old type of using "generic" methods
    public static void printArrayV5(Object[] array){
        for (int i=0;i<array.length;i++)
            System.out.println(array[i]);
    }
    public static void main(String... args){
        Integer[] test1 = {2,67,22,12,3,6};
        //printArrayV1(test1);
        String[] test2 = {"Ornela", "Fitore", "Arber", "Driton", "Efthimios"};
        //printArrayV2(test2);
        //printArrayV4(test1);
        //test2[5] = "John"; //no way, Exception
        ArrayList<String> newTest2 = makeList(test2);
        newTest2.add("John");
        GenClass<Integer,String,Child> genClass1 = new GenClass<>();
        genClass1.setVal(1234098765);
        System.out.println(genClass1.getVal());
    }
    public static <T> ArrayList<T> makeList(T[] array){
        ArrayList<T> newList = new ArrayList<>();
        for (T t :
                array) {
            newList.add(t);
        }
        return newList;
    }
}
