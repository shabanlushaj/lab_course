package com.unipi.talepis;

public class GenClass<T,E,V extends Parent> {
    private T val;

    public T getVal() {
        return val;
    }
    
    public void doSomething(V v){
        System.out.println(v.getPriv_Num());
    }

    public void setVal(T val) {
        if (val.toString().length()>5)
            this.val = val;
    }
}
