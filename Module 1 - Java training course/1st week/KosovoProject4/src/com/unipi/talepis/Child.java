package com.unipi.talepis;

public class Child extends Parent{
    public Child() {
        System.out.println("Hello from Child!");
    }

    public void showInfo(){
        System.out.println(this.hashCode());
        System.out.println(super.hashCode());
        System.out.println(this.getClass());
        System.out.println(super.getClass());
    }
}
