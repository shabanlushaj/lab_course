package com.unipi.talepis;

public interface GenericInterface<T> {
    void performAction(T action);
}
