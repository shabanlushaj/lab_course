package com.unipi.talepis;

import java.util.Date;

public class Student {
    private int ID;
    private String name;
    Date birthday;

    // Shallow copy
    public Student(Student student) {    // this is a Copy Constructor
        this.ID = student.ID;
        this.name = student.name;   // no problem with copying because
                                    // Strings are IMMUTABLE!!!!!
        // Deep copy
        this.birthday = new Date(student.birthday.getTime());
    }
}
