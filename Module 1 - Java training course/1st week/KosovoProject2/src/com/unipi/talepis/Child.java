package com.unipi.talepis;

public class Child extends Father/*,Mother*/implements ISpeak{
    @Override
    public void speak(String s) {
        System.out.println("I am a single child... And I say "+s);
    }
}
