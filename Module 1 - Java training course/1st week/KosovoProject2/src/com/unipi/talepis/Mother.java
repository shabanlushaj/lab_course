package com.unipi.talepis;

public class Mother extends Human implements ISpeak{
    @Override
    public void speak(String s) {
        System.out.println("I am a mother! And I want to tell you "+s);
    }

    @Override
    public String sayYourName() {
        return "Mother";
    }
}
