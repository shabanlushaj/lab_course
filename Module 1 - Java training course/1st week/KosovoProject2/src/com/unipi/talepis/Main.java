package com.unipi.talepis;

public class Main {

    public static void main(String[] args) {
	    /*Human h1 = new Human();
        System.out.println(h1.toString());
        Student s1 = new Student();*/
        /*System.out.println(s1.hashCode());
        Student s2 = s1;
        System.out.println(s2.hashCode());
        Student s3 = new Student();
        System.out.println(s3.hashCode());*/
        /*System.out.println(s1.sayYourName());
        Child c1 = new Child();
        System.out.println(c1.sayYourName());*/
        /*Human h2 = new Human();
        h2.name = "Bardha";
        System.out.println(h2.sayYourName());
        Human.doSomething();*/
        Student s4 = new Student();
        s4.name = "Driton";
        /*s4.age = 1500;
        s4.height = -24.56f;
        s4.email = "unipi";
        s4.tel = "Kosovo";*/
        /*s4.setAge(200);
        System.out.println(s4.getAge());
        s4.setAge(41);
        System.out.println(s4.getAge());*/
        Course c1 = new Course();
        Student s5 = new Student();
        Mother m1 = new Mother();
        Human h6 = new Human();
        Father f1 = new Father();
        Child c7 = new Child();
        c1.teach(s5);
        c1.teach(m1);
        //c1.teach(h6); not acceptable
        //c1.teach(f1); not acceptable
        c1.teach(c7);
   }
}
