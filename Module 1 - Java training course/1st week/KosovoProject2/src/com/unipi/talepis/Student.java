package com.unipi.talepis;

public class Student extends Human implements ISpeak{
    private String email;
    private int age;
    private String tel;
    private float height;

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age>=0 && age<120)
            this.age = age;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public String getTel() {
        return tel;
    }
    @Override
    public void speak(String s) {
        System.out.println("I am a student! And I say: "+s);
    }
}
