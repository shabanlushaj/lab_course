package com.unipi.talepis.kosovoproject31;

import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DBController {
    private static  String url = "jdbc:sqlite:E:\\Dropbox\\java\\intellijprojects\\kosovoproject31\\src\\main\\resources\\DB\\MYSQLITEDB.db";

    @RequestMapping("/allmodels")
    public String selectModels(){
        StringBuilder builder = new StringBuilder();
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select Model from Smartphone");

            while (resultSet.next()){
                builder.append(resultSet.getString("Model")).append("<br/>");
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return builder.toString();
    }
    @RequestMapping("/fetchsmartphones")
    public List<Smartphone> fetchSmartphones(){
        List<Smartphone> smartphones = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from Smartphone");

            while (resultSet.next()){
                Smartphone smartphone = new Smartphone();
                smartphone.setId(resultSet.getString("ID"));
                smartphone.setModel(resultSet.getString("Model"));
                smartphone.setDescription(resultSet.getString("Description"));
                smartphone.setPrice(resultSet.getInt("Price"));
                smartphones.add(smartphone);
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return smartphones;
    }
    @GetMapping("/smartphone/{id}")
    public Smartphone show(@PathVariable String id) {
        Smartphone smartphone = null;
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement =
                    connection.prepareStatement("Select * From Smartphone Where id=?");
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                smartphone = new Smartphone();
                smartphone.setModel(resultSet.getString("Model"));
                smartphone.setDescription(resultSet.getString("Description"));
                smartphone.setPrice(resultSet.getInt("Price"));
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return smartphone;
    }
    @DeleteMapping("/smartphone/{id}")
    public Map<String,String> delete(@PathVariable String id){
        String message = null;
        try {
            Connection connection = DriverManager.getConnection(url);
            PreparedStatement statement = connection.prepareStatement("Delete From Smartphone Where ID=?");
            statement.setString(1,id);
            if (statement.executeUpdate()>0){
                message = "Smartphone deleted...";
            }else {
                message = "Not valid id";
            }
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String finalMessage = message;
        return new HashMap<>(){{put("message", finalMessage);}};
    }
}
