import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Form1 extends JFrame{
    private JPanel panl1;
    private JButton button1;
    private JLabel imageLabel1;
    private JButton saveButton;

    public Form1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setContentPane(panl1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        try
        {
            FileInputStream serviceAccount =
                    new FileInputStream("src/main/resources/firebasekey/kosovoproject27-firebase-adminsdk-l6m7q-0fe37146a5.json");
            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setStorageBucket("kosovoproject27.appspot.com")
                    .build();
            FirebaseApp.initializeApp(options);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try
                {
                    Storage storage =
                            StorageOptions.newBuilder()
                            .setCredentials(
                                    ServiceAccountCredentials.fromStream(
                                            new FileInputStream("src/main/resources/firebasekey/kosovoproject27-firebase-adminsdk-l6m7q-0fe37146a5.json")
                                    )
                            )
                            .build()
                            .getService();
                    Blob blob = storage.get(BlobId.of("kosovoproject27.appspot.com","sun.png"));
                    blob.downloadTo(Paths.get("tmp.png"));
                    imageLabel1.setText("");
                    loadImage("tmp.png",imageLabel1);
                } catch (IOException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
            }
        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{
                    try
                    {
                        Storage storage =
                                StorageOptions.newBuilder()
                                        .setCredentials(
                                                ServiceAccountCredentials.fromStream(
                                                        new FileInputStream("src/main/resources/firebasekey/kosovoproject27-firebase-adminsdk-l6m7q-0fe37146a5.json")
                                                )
                                        )
                                        .build()
                                        .getService();
                        BlobId blobId = BlobId.of("kosovoproject27.appspot.com","cat.jpg");
                        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
                        storage.create(blobInfo,Files.readAllBytes(Paths.get("src/main/resources/images/cat.jpg")));
                    } catch (IOException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    }
                }).start();
            }
        });
    }
    private void loadImage(String path, JLabel pictureLabel){
        pictureLabel.setText("");
        pictureLabel.setIcon(null);
        try {
            BufferedImage img = ImageIO.read(new File(path));
            Image resized = img.getScaledInstance(pictureLabel.getWidth(),pictureLabel.getHeight(),
                    Image.SCALE_SMOOTH);
            pictureLabel.setIcon(new ImageIcon(resized));
            pictureLabel.revalidate();
            pictureLabel.repaint();
            pictureLabel.update(pictureLabel.getGraphics());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        new Form1();
    }
}
