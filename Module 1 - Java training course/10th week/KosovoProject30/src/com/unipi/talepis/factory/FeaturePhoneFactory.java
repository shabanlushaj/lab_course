package com.unipi.talepis.factory;

public class FeaturePhoneFactory implements PhoneAbstractFactory{
    private int screenSize;
    private String storage;
    private String phoneNumber;

    public FeaturePhoneFactory(int screenSize, String storage, String phoneNumber) {
        this.screenSize = screenSize;
        this.storage = storage;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public Phone createPhone() {
        return new FeaturePhone(screenSize,storage,phoneNumber);
    }
}
