package com.unipi.talepis.factory;

import org.w3c.dom.ls.LSOutput;

public class DemoFactoryTest {
    public static void main(String[] args) {
        Phone phone1 = PhoneFactory.getPhone(
                new FeaturePhoneFactory(2,"internal","98765456"));
        Phone phone2 = PhoneFactory.getPhone(
                new SmartPhoneFactory(10,"SD cart","+30764565445654"));
        System.out.println(phone1.toString());
        System.out.println(phone2.toString());
    }
}
