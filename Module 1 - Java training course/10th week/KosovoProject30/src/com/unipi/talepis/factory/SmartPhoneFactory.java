package com.unipi.talepis.factory;

public class SmartPhoneFactory implements PhoneAbstractFactory{
    private int screenSize;
    private String storage;
    private String phoneNumber;

    public SmartPhoneFactory(int screenSize, String storage, String phoneNumber) {
        this.screenSize = screenSize;
        this.storage = storage;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public Phone createPhone() {
        return new SmartPhone(screenSize,storage,phoneNumber);
    }
}
