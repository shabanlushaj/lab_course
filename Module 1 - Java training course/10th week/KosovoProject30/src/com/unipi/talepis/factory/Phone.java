package com.unipi.talepis.factory;

public abstract class Phone {
    public abstract int getScreenSize();
    public abstract String getStorage();
    public abstract String getPhoneNumber();

    @Override
    public String toString() {
        return "ScreenSize:"+this.getScreenSize()+"\n"
                +"Storage:"+this.getStorage()+"\n"
                +"Phone Number:"+this.getPhoneNumber();
    }
}
