package com.unipi.talepis.factory;

public interface PhoneAbstractFactory {
    Phone createPhone();
}
