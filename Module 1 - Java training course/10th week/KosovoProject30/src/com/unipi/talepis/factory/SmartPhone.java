package com.unipi.talepis.factory;

public class SmartPhone extends Phone{
    private int screenSize;
    private String storage;
    private String phoneNumber;
    //more fields regarding smart phones

    // more methods
    public SmartPhone(int screenSize, String storage, String phoneNumber) {
        this.screenSize = screenSize;
        this.storage = storage;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int getScreenSize() {
        return screenSize;
    }

    @Override
    public String getStorage() {
        return storage;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

}
