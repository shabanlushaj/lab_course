package com.unipi.talepis;

import java.util.HashMap;
import java.util.Map;

public class Multiton {
    private Multiton(){}
    private static final Map<String,Multiton> multitonInstance =
            new HashMap<>();
    public static Multiton getInstance(String key){
        Multiton instance = multitonInstance.get(key);
        if (instance==null){
            synchronized (Multiton.class){
                if (instance==null){
                    instance = new Multiton();
                    multitonInstance.put(key,instance);
                }
            }
        }
        return instance;
    }
}
