package com.unipi.talepis;

import javax.swing.*;
import javax.swing.plaf.nimbus.State;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class Form1 extends JFrame{
    private JPanel panel1;
    private JTextField textField1;
    private JPasswordField passwordField1;
    private JButton okButton;
    public Form1(){
        setTitle("Login Form");
        setPreferredSize(new Dimension(220,250));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:sqlite:kosovo16.db");
            Statement statement = connection.createStatement();
            String createSQL = "CREATE TABLE IF NOT EXISTS USER" +
                    "(ID INTEGER NOT NULL," +
                    "USER_NAME TEXT," +
                    "USER_PASS TEXT," +
                    "PRIMARY KEY (ID))";
            statement.executeUpdate(createSQL);
            String insertSQL = "INSERT OR IGNORE INTO USER " +
                    "VALUES(1,'talepis','12345')";
            statement.executeUpdate(insertSQL);
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Form1.this.loginV1();
                Form1.this.loginV2DefendSQLInjection();
            }
        });
    }
    void loginV1(){
        String loginSQL = "SELECT ID " +
                "FROM USER " +
                "WHERE USER_NAME='" + textField1.getText()+
                "' AND USER_PASS='" + new String(passwordField1.getPassword())+
                "'";
        try(Connection connection = connect();
            Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(loginSQL);
            if (resultSet.next()){
                //JOptionPane.showMessageDialog(null,"Login success!");
                new Form2(resultSet.getString("ID"));
            }else {
                JOptionPane.
                        showMessageDialog(null,"Wrong username and/or password");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    void loginV2DefendSQLInjection(){
        String loginSQL = "SELECT ID " +
                "FROM USER " +
                "WHERE USER_NAME=? " +
                "AND USER_PASS=?";
        try(Connection connection = connect();
            PreparedStatement statement = connection.prepareStatement(loginSQL)){
            statement.setString(1,textField1.getText());
            statement.setString(2, new String(passwordField1.getPassword()));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                //JOptionPane.showMessageDialog(null,"Login success!");
                new Form2(resultSet.getString("ID"));
            }else {
                JOptionPane.
                        showMessageDialog(null,"Wrong username and/or password");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    static Connection connect(){
        String url = "jdbc:sqlite:kosovo16.db";
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
