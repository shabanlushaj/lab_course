package com.unipi.talepis;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Form2 extends JFrame{
    private JTextArea textArea1;
    private JTextField textField1;
    private JTextField textField2;
    private JLabel pictureLabel;
    private JPanel panel1;

    public Form2(String id){
        setTitle("Login Form");
        setPreferredSize(new Dimension(500,400));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        retrieveInfo(id);
    }
    private void retrieveInfo(String id){
        String sql = "SELECT PERSONAL_DATA,EMAIL,TELEPHONE,IMAGE " +
                "FROM USER_INFO " +
                "WHERE ID="+id;
        try(Connection connection = Form1.connect();
            Statement statement = connection.createStatement()){
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()){
                textArea1.setText(rs.getString("PERSONAL_DATA"));
                textField1.setText(rs.getString("EMAIL"));
                textField2.setText(rs.getString("TELEPHONE"));
                loadImage("images/"+rs.getString("IMAGE"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void loadImage(String path){
        pictureLabel.setIcon(null);
        try {
            BufferedImage img = ImageIO.read(new File(path));
            Image resized = img.getScaledInstance(pictureLabel.getWidth(),pictureLabel.getHeight(),
                    Image.SCALE_SMOOTH);
            pictureLabel.setIcon(new ImageIcon(resized));
            pictureLabel.revalidate();
            pictureLabel.repaint();
            pictureLabel.update(pictureLabel.getGraphics());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
