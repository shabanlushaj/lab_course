package com.unipi.talepis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class Form1 extends JFrame {
    private JPanel panel1;
    Graphics2D g;
    int prevX,prevY;
    Point previousPoint;
    boolean draw = false;

    public Form1(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500,500));
        setContentPane(panel1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        g = (Graphics2D)panel1.getGraphics();
        previousPoint = new Point();
        panel1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //g.drawLine(0,0,e.getX(),e.getY());
                g.setColor(Color.RED);
                g.drawOval(e.getX()-5,e.getY()-5,10,10);
                g.setColor(panel1.getBackground());
                g.drawOval(previousPoint.x-5,previousPoint.y-5,10,10);
                previousPoint.x = e.getX();
                previousPoint.y = e.getY();
                //g.setColor(Color.RED);
                //g.drawRect(e.getX()-15,e.getY()-15,30,30);
            }
            @Override
            public void mousePressed(MouseEvent e) {
                g.setStroke(new BasicStroke(3));
                g.setColor(Color.MAGENTA);
                prevX = e.getX();
                prevY = e.getY();
                draw = true;
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                draw = false;
            }
        });
        panel1.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (draw){
                    g.drawLine(prevX,prevY,e.getX(),e.getY());
                    prevX = e.getX();
                    prevY = e.getY();
                }
            }
        });
    }
}
