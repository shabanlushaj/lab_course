package com.unipi.talepis;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.*;

public class Form1 extends JFrame {
    private JPanel panel1;
    private JTable table1;

    public Form1(){
        setTitle("Login Form");
        setPreferredSize(new Dimension(380,450));
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        try {
            Connection connection =
                    DriverManager.getConnection("jdbc:sqlite:kosovo18.db");
            Statement statement = connection.createStatement();
            String select = "SELECT USER_NAME,USER_PASS FROM USER";
            ResultSet rs = statement.executeQuery(select);
            DefaultTableModel model = new DefaultTableModel();
            table1.setModel(model);
            model.addColumn("User Name");
            model.addColumn("Password");
            while (rs.next()){
                model.addRow(new Object[]{rs.getString("USER_NAME"),rs.getString("USER_PASS")});
            }
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        table1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting())
                    System.out.println(table1.getValueAt(table1.getSelectedRow(),0).toString());
            }
        });

    }
}
