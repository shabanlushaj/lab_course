package com.unipi.talepis;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

public class Form1 extends JFrame{
    private JPanel panel1;
    private JLabel label1;
    private JButton mainThreadButton;
    private JButton threadButton;
    public Form1() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(500, 500));
        setContentPane(panel1);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        mainThreadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i=0;i<10;i++){
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    label1.setText(String.valueOf(i));
                }
            }
        });
        threadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(()->{
                    for (int i=0;i<10;i++){
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                        label1.setText(String.valueOf(i));
                    }
                }).start();
            }
        });
    }

}
