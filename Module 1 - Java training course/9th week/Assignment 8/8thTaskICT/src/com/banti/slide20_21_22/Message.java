package com.banti.slide20_21_22;

public class Message {
    private String msg;

    public Message(String msg) {
        this.msg=msg;
    }

    public String getMsg() {
        return this.msg;
    }
}
