package com.banti.slide36_41;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecService {
    // Run threads
    public static void main(String[] args) {
        System.out.println("System time: " + System.currentTimeMillis() + " --- " + Thread.currentThread().getName() + " > start");

        ExecutorService executor = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 5; i++) {
            Runnable thread = new Runnable() {
                public void run() {
                    Random random = new Random();
                    for (int i = 0; i < 5; i++) {
                        System.out.println("System time: " + System.currentTimeMillis() + " --- " + Thread.currentThread().getName() + " random number:  " + random.nextInt(1000));
                    }
                }
            };

            executor.execute(thread);

        }

        executor.shutdown();

        while (!executor.isTerminated()) {
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("System time: " + System.currentTimeMillis() + " --- " + Thread.currentThread().getName() + " > done");
    }

}
