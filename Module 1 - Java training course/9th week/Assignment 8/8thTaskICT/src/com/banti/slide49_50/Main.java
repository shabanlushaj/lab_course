package com.banti.slide49_50;

import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) {
        ForkJoinPool fjpObj = new ForkJoinPool();

        double[] nums = new double[400];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = ((i % 2) == 0) ? i : -1;
        }

        Sum task = new Sum(nums, 0, nums.length);
        double summation = fjpObj.invoke(task);
        System.out.println("Summation " + summation);

    }
}

