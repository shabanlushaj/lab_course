package com.unipi.talepis;

public class LazySignletonV3 {
    private LazySignletonV3(){}
    private static class SingletonHolder{
        static LazySignletonV3 onDemandSigleton = new LazySignletonV3();
    }
    public static LazySignletonV3 getInstance(){
        return SingletonHolder.onDemandSigleton;
    }
}
