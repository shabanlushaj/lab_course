package com.unipi.talepis;

public class EagerSingletonDefend1 {
        private static final EagerSingletonDefend1 instance =
                new EagerSingletonDefend1();
        private EagerSingletonDefend1(){
            if (instance!=null){
                throw new IllegalStateException("instance is already created!");
            }
        };
        public static EagerSingletonDefend1 getInstance(){
            return instance;
        }
}
