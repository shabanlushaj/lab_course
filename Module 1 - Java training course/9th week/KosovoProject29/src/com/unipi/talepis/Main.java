package com.unipi.talepis;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) {
        //simpleDemo();
        //attackToEager1();
        attackToEager2();
    }
    static void simpleDemo(){
        EagerSingleton e1 = EagerSingleton.getInstance();
        EagerSingleton e2 = EagerSingleton.getInstance();
        System.out.println(e1.hashCode());
        System.out.println(e2.hashCode());
        System.out.println("-----------------------");
        LazySingleton l1 = LazySingleton.getInstance();
        LazySingleton l2 = LazySingleton.getInstance();
        System.out.println(l1.hashCode());
        System.out.println(l2.hashCode());
        System.out.println("-----------------------");
        LazySignletonV3 l3 = LazySignletonV3.getInstance();
        LazySignletonV3 l4 = LazySignletonV3.getInstance();
        System.out.println(l3.hashCode());
        System.out.println(l4.hashCode());
        System.out.println("-----------------------");
    }
    static void attackToEager1(){
        EagerSingleton e1 = EagerSingleton.getInstance();
        EagerSingleton e2 = null;
        try {
            Constructor constructor =
                    EagerSingleton.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            e2 = (EagerSingleton) constructor.newInstance();
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(e1.hashCode());
        System.out.println(e2.hashCode());
        System.out.println("-----------------------");
    }
    static void attackToEager2(){
        EagerSingletonDefend1 e1 = EagerSingletonDefend1.getInstance();
        EagerSingletonDefend1 e2 = null;
        try {
            Constructor constructor =
                    EagerSingletonDefend1.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            e2 = (EagerSingletonDefend1) constructor.newInstance();
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println(e1.hashCode());
        System.out.println(e2.hashCode());
        System.out.println("-----------------------");
    }
}
