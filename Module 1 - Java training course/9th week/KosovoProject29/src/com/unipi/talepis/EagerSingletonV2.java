package com.unipi.talepis;

public class EagerSingletonV2 {
    private static EagerSingletonV2 instance;
    private EagerSingletonV2(){}
    static {
        try {
            instance = new EagerSingletonV2();
        }catch (Exception e){

        }
    }
    public static EagerSingletonV2 getInstance(){
        return instance;
    }
}
