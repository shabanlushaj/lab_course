package com.unipi.talepis;

public class LazySingletonV2 {
    private static LazySingletonV2 instance;
    private LazySingletonV2(){}
    public static LazySingletonV2 getInstance(){
        // double check locking
        if (instance==null){
            synchronized (LazySingletonV2.class){
                if (instance==null)
                    instance = new LazySingletonV2();
            }
        }
        return instance;
    }
}
