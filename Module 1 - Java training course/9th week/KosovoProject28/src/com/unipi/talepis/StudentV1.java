package com.unipi.talepis;


// Using Telescoping Constructor Design Pattern

public class StudentV1 {
    private final String email;
    private final String AM;
    private final String name;
    private final String telephone;
    private final boolean undergraduate;
    private final int year;

    public StudentV1(String email, String AM, String name, String telephone, boolean undergraduate, int year) {
        this.email = email;
        this.AM = AM;
        this.name = name;
        this.telephone = telephone;
        this.undergraduate = undergraduate;
        this.year = year;
    }
    public StudentV1(String email, String AM, String name, String telephone, boolean undergraduate) {
        this(email,AM,name,telephone,undergraduate,2020);
    }
    public StudentV1(String email, String AM, String name, String telephone) {
        this(email,AM,name,telephone,true,2020);
    }
    public StudentV1(String email, String AM, String name) {
        this(email,AM,name,"000000",true,2020);
    }
    public StudentV1(String email, String AM) {
        this(email,AM,"No-Name","000000",true,2020);
    }

}
