package com.unipi.talepis;

public class Main {

    public static void main(String[] args) {
	    StudentV1 stdV1 = new StudentV1("talepis@gmail.com","AM123");
	    StudentV2 stdV2 = new StudentV2("Driton@yahoo.com","AM567");
	    stdV2.setYear(2018);
	    StudentV3 stdV3 = new StudentV3.Builder("arlinda@gmail.com","AM098")
				.undergraduate(false)
				.telephone("67895436789")
				.year(2019)
				.build();
    }
}
