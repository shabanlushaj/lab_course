package com.unipi.talepis;

//Builder Design Pattern

public class StudentV3 {
    private final String email;
    private final String AM;
    private final String name;
    private final String telephone;
    private final boolean undergraduate;
    private final int year;

    private StudentV3(Builder builder){
        this.email = builder.email;
        this.AM = builder.AM;
        this.name = builder.name;
        this.telephone = builder.telephone;
        this.undergraduate = builder.undergraduate;
        this.year = builder.year;
    }
    public static class Builder{
        private final String email;
        private final String AM;
        private String name;
        private String telephone;
        private boolean undergraduate;
        private int year;

        public Builder(String email, String AM) {
            this.email = email;
            this.AM = AM;
        }
        public Builder name(String name){
            this.name = name;
            return this;
        }
        public Builder telephone(String telephone){
            this.telephone = telephone;
            return this;
        }
        public Builder undergraduate(boolean undergraduate){
            this.undergraduate = undergraduate;
            return this;
        }
        public Builder year(int year){
            this.year = year;
            return this;
        }
        public StudentV3 build(){
            return new StudentV3(this);
        }
    }
}
