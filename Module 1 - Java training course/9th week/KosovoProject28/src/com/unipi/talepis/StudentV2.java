package com.unipi.talepis;

//JavaBeans Design Pattern

public class StudentV2 {
    private final String email; //required
    private final String AM;    //required
    private String name;
    private String telephone;
    private boolean undergraduate;
    private int year;

    public String getEmail() {
        return email;
    }

    public String getAM() {
        return AM;
    }

    public String getName() {
        return name;
    }

    public String getTelephone() {
        return telephone;
    }

    public boolean isUndergraduate() {
        return undergraduate;
    }

    public int getYear() {
        return year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setUndergraduate(boolean undergraduate) {
        this.undergraduate = undergraduate;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public StudentV2(String email, String AM) {
        this.email = email;
        this.AM = AM;
    }
}
