package com.unipi.talepis.test432;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity2 extends AppCompatActivity {
    EditText editText3,editText4;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editText3 = findViewById(R.id.editTextTextPersonName3);
        editText4 = findViewById(R.id.editTextTextPersonName4);
        auth = FirebaseAuth.getInstance();
    }
    public void signup(View view){
        auth.createUserWithEmailAndPassword(editText3.getText().toString(),
                editText4.getText().toString()).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_LONG).show();
                        }else {
                            String error = task.getException().getMessage();
                        }
                    }
                });
    }
    public void signin(View view){
        auth.signInWithEmailAndPassword(editText3.getText().toString(),
                editText4.getText().toString()).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            String userID = auth.getCurrentUser().getUid();
                            Toast.makeText(getApplicationContext(),userID,Toast.LENGTH_LONG).show();
                        }else {
                            String error = task.getException().getMessage();
                        }
                    }
                });
    }

}