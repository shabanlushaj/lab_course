package com.unipi.talepis.kosovoandroid11;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    TextView textView;
    SensorManager manager;
    Sensor s1,s2;
    int stepCounter, stepDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        s1 = manager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        s2 = manager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
    }
    public void goAsync(View view){
        MyAsync myAsync = new MyAsync(textView);
        myAsync.execute("Altin","Morina");
    }

    public void countSteps(View view){
        manager.registerListener(this,s1,SensorManager.SENSOR_DELAY_NORMAL);
        manager.registerListener(this,s2,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType()==Sensor.TYPE_STEP_COUNTER)
            stepCounter++;
        if (sensorEvent.sensor.getType()==Sensor.TYPE_STEP_DETECTOR)
            stepDetector++;
        //float countedSteps = sensorEvent.values[0]; //try this as well
        textView.setText("Counted steps:"+String.valueOf(sensorEvent.values[0])+"\n"+
                "Detected steps:"+String.valueOf(stepDetector));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}