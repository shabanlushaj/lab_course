package com.unipi.talepis.kosovoandroid11;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class MyAsync extends AsyncTask<String,Integer,String> {
    WeakReference<TextView> textViewWeakReference;
    public MyAsync(TextView textView) {
        textViewWeakReference = new WeakReference<>(textView);
    }

    @Override
    protected void onPreExecute() {
        textViewWeakReference.get().setText("Background job started!");
    }

    @Override
    protected void onPostExecute(String s) {
        textViewWeakReference.get().setText(s);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        textViewWeakReference.get().setText(String.valueOf(values[0]*20)+"% done");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(String... strings) {
        for (int i =0;i<5;i++){
            SystemClock.sleep(1000);
            publishProgress(i+1);
        }
        return "Hello "+strings[0]+" "+strings[1];
    }
}
