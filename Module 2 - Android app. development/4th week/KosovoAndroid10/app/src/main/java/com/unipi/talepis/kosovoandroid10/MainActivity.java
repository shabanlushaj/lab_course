package com.unipi.talepis.kosovoandroid10;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
    }
    public void go1(View view){
        new Thread(()->{
            for (int i=1;i<10;i++){
                try {
                    Thread.sleep(1000);
                    int finalI = i;
                    runOnUiThread(()->textView.setText(String.valueOf(finalI)));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            playNotificationSound();
        }).start();
    }
    private void playNotificationSound(){
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(),notification);
        ringtone.play();
    }
    public void go2(View view){
        CountDownTimer timer = new CountDownTimer(10000,1000) {
            Integer counter = new Integer(0);
            @Override
            public void onTick(long l) {
                counter++;
                textView.setText(counter.toString());
            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(),"Finished!",Toast.LENGTH_LONG).show();
            }
        };
        timer.start();
    }
    public void go3(View view){
        long alarmTime = System.currentTimeMillis()+10000;
        Intent intent = new Intent(this,MyService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this,54,intent,0);
        AlarmManager manager = (AlarmManager)getSystemService(ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP,alarmTime,pendingIntent);
        //manager.cancel(pendingIntent);
    }
}