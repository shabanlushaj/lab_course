package com.unipi.talepis.kosovoandroid9;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText1, editText2;
    private static final int BTN1 = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.editTextTextPersonName);
        editText2 = findViewById(R.id.editTextTextPersonName2);
    }
    public void sms(View view){
        String phoneNumber = editText1.getText().toString();
        String message = editText2.getText().toString();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)!=
                PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},234);
        }else {
            SmsManager manager = SmsManager.getDefault();
            manager.sendTextMessage(phoneNumber,null,message,null,null);
            Toast.makeText(this,"Message is sent!",Toast.LENGTH_LONG).show();
            editText2.setText("");
        }
    }
    public void create(View view){
        ConstraintLayout constraintLayout = findViewById(R.id.cl1);
        Button button = new Button(this);
        button.setText("Press me!");
        button.setId(BTN1);
        button.setLayoutParams(
                new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT,
                        ConstraintLayout.LayoutParams.WRAP_CONTENT));
        button.setOnClickListener((v)->{
            Toast.makeText(getApplicationContext(),"I am pressed!",Toast.LENGTH_LONG).show();
        });
        constraintLayout.addView(button);
    }
}