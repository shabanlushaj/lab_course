package com.unipi.talepis;

import java.util.List;

public class ChainValidator {
    public static Boolean isChainValid(int prefix, List<Block> blockChain){
        Block currentBlock;
        Block previousBlock;
        String prefixString = new String(new char[prefix]).replace('\0','0');
        for(int i = 1; i<blockChain.size();i++){
            currentBlock = blockChain.get(i);
            previousBlock = blockChain.get(i-1);
            if (!currentBlock.getHash().equals(currentBlock.calculateBlockHash())){
                return false;
            }
            if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())){
                return false;
            }
            if (!currentBlock.getHash().substring(0,prefix).equals(prefixString)){
                return false;
            }
        }
        return true;
    }
}
