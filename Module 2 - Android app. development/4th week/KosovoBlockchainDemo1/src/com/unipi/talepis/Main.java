package com.unipi.talepis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static List<Block> blockChain = new ArrayList<>();
    public static int prefix = 6;

    public static void main(String[] args) {
	    long startTime = System.nanoTime();
        System.out.println("Process started");

        //Genesis Block
        Block genesisBlock = new Block("0","Data of genesis block",new Date().getTime());
        genesisBlock.mineBlock(prefix);
        blockChain.add(genesisBlock);
        System.out.println("Node:"+(blockChain.size()-1)+" created");

        Block secondBlock = new Block(blockChain.get(blockChain.size()-1).getHash(),"Data of second block",new Date().getTime());
        genesisBlock.mineBlock(prefix);
        blockChain.add(secondBlock);
        System.out.println("Node:"+(blockChain.size()-1)+" created");

        Block thirdBlock = new Block(blockChain.get(blockChain.size()-1).getHash(),"Data of third block",new Date().getTime());
        genesisBlock.mineBlock(prefix);
        blockChain.add(thirdBlock);
        System.out.println("Node:"+(blockChain.size()-1)+" created");

        long endTime = System.nanoTime();
        long duration = endTime-startTime;
        System.out.println("Duration:"+(float)duration/1000000000+" seconds");
        System.out.println("Is BlockChain valid?");
        System.out.println(ChainValidator.isChainValid(prefix,blockChain));
    }
}
