package com.banti.androidtaskone_ict;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText id, number, name, email, address;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id = findViewById(R.id.userId);
        number = findViewById(R.id.userNumber);
        name = findViewById(R.id.userName);
        email = findViewById(R.id.userEmail);
        address = findViewById(R.id.userAddress);
        db = openOrCreateDatabase("AndroidUsers", Context.MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS USER(user_id TEXT, user_name TEXT, user_tel TEXT, user_email TEXT, user_address TEXT)");

    }

    public void insert(View view){
        String dbId = id.getText().toString();
        String dbName = name.getText().toString();
        String dbNumber = number.getText().toString();
        String dbEmail = email.getText().toString();
        String dbAddress = address.getText().toString();

        if(!id.getText().toString().equals("") && !number.getText().toString().equals("")) {
            db.execSQL("INSERT INTO USER VALUES (?,?,?,?,?)", new String[]{dbId, dbNumber, dbName, dbEmail, dbAddress});
            Toast.makeText(this, "Inserted successfully", Toast.LENGTH_LONG).show();
            name.setText("");
            id.setText("");
            number.setText("");
            email.setText("");
            address.setText("");
        }
        else
            Toast.makeText(this,"Provide values in required fields",Toast.LENGTH_LONG).show();

    }
    public void select(View view){
        String dbId = id.getText().toString();
        String dbName = name.getText().toString();
        String dbNumber = number.getText().toString();


        if(dbId.isEmpty() && dbNumber.isEmpty()&&dbName.isEmpty())
        {
            retrieveData(db.rawQuery("SELECT * FROM USER",null));
        }
        else if(!dbId.isEmpty())
        {
            Cursor cursor = db.rawQuery("Select * from USER where user_id = ?",new String[]{dbId});
            while (cursor.moveToNext())
            {
                number.setText(cursor.getString(1));
                name.setText(cursor.getString(2));
                email.setText(cursor.getString(3));
                address.setText(cursor.getString(4));
            }

            cursor.close();
        }
        else if(!dbName.isEmpty())
        {
            retrieveData(db.rawQuery("SELECT * FROM USER WHERE user_name = ?" , new String[]{dbName}));
        }
        else if(!dbNumber.isEmpty())
        {
            retrieveData(db.rawQuery("SELECT * FROM USER WHERE user_tel = ?" , new String[]{dbNumber}));
        }
    }

    public void retrieveData(Cursor cursor){

        StringBuilder sb = new StringBuilder();
        while (cursor.moveToNext()){
            sb.append("User ID: ").append(cursor.getString(0)).append("\n");
            sb.append("Number: ").append(cursor.getString(1)).append("\n");
            sb.append("Name: ").append(cursor.getString(2)).append("\n");
            sb.append("Email: ").append(cursor.getString(3)).append("\n");
            sb.append("Address: ").append(cursor.getString(4)).append("\n-----------------------\n");

        }
        showMessage("User records",sb.toString());
    }
    public void update(View view){
        String dbId = id.getText().toString();
        String dbName = name.getText().toString();
        String dbNumber = number.getText().toString();
        String dbEmail = email.getText().toString();
        String dbAddress = address.getText().toString();

        if(!id.getText().toString().equals("")) {
            db.execSQL("update USER set user_tel = ?, user_name = ?, user_email =?, user_address = ? where user_id = ? ", new String[]{dbNumber, dbName, dbEmail, dbAddress, dbId});
            Toast.makeText(this, "Updated successfully", Toast.LENGTH_LONG).show();
            name.setText("");
            id.setText("");
            number.setText("");
            email.setText("");
            address.setText("");
        }
        else
            Toast.makeText(this, "Provide an ID", Toast.LENGTH_LONG).show();

    }

    public void delete(View view)
    {
        if(!id.getText().toString().equals("")) {
            db.execSQL("DELETE FROM USER where user_id = ? ", new String[]{id.getText().toString()});
            Toast.makeText(this, "Deleted successfully", Toast.LENGTH_LONG).show();
            name.setText("");
            id.setText("");
            number.setText("");
            email.setText("");
            address.setText("");
        }
        else
            Toast.makeText(this, "Provide an ID", Toast.LENGTH_LONG).show();
    }

    public void showMessage(String title, String message){
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .show();
    }

}