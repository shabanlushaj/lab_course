package com.unipi.talepis.kosovoandroid3v2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==VOICE_REC_RESULT && resultCode==RESULT_OK){
            List<String> strings = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            StringBuilder builder = new StringBuilder();
            for(String s:strings){
                builder.append(s).append("\n");
            }
            //showMessage("Recognized Text",builder.toString());
            List<String> colorList = new ArrayList<>();
            colorList.add("blue");
            colorList.add("red");
            colorList.add("yellow");
            colorList.add("green");
            colorList.add("white");
            for (String color:strings){
                if (colorList.contains(color.toLowerCase())){
                    getWindow().getDecorView().setBackgroundColor(Color.parseColor(color.toLowerCase()));
                    return;
                }
            }
            for (String name:strings){
                if (selectoneByname(name))
                    //break;
                    return;
            }
            if (strings.get(0).toLowerCase().equals("google map")){
                startActivity(new Intent(this,MapsActivity.class));
            }
        }
    }

    SQLiteDatabase db;
    EditText editText1,editText2,editText3;
    private static final int VOICE_REC_RESULT = 543;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editText1 = findViewById(R.id.editTextTextPersonName1);
        editText2 = findViewById(R.id.editTextTextPersonName2);
        editText3 = findViewById(R.id.editTextTextPersonName3);
        db = openOrCreateDatabase("DBUSERS", Context.MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS USER(user_id TEXT,user_name TEXT,user_tel TEXT)");
    }
    public void insert(View view){
        db.execSQL("INSERT INTO USER VALUES('"+editText1.getText()+"','"+editText2.getText()+"','"+editText3.getText()+"')");
        Toast.makeText(this,"Record inserted successfully!",Toast.LENGTH_LONG).show();
    }
    public void insertV2(View view){
        String id = editText1.getText().toString();
        String name = editText2.getText().toString();
        String tel = editText3.getText().toString();
        db.execSQL("INSERT INTO USER VALUES(?,?,?)",new String[]{id,name,tel});
        Toast.makeText(this,"Record inserted successfully!",Toast.LENGTH_LONG).show();
    }
    public void select(View view){
        Cursor cursor = db.rawQuery("SELECT * FROM USER WHERE user_id!=?",new String[]{"0"});
        StringBuilder  builder = new StringBuilder();
        while (cursor.moveToNext()){
            builder.append("User ID:").append(cursor.getString(0)).append("\n");
            builder.append("Name:").append(cursor.getString(1)).append("\n");
            builder.append("Telephone:").append(cursor.getString(2)).append("\n");
            builder.append("-----------------------------------\n");
        }
        showMessage("User Records",builder.toString());
    }
    public void selectone(View view){
        String id = editText1.getText().toString();
        Cursor cursor = db.rawQuery("SELECT * FROM USER WHERE user_id=?",new String[]{id});
        StringBuilder  builder = new StringBuilder();
        while (cursor.moveToNext()){
            builder.append("User ID:").append(cursor.getString(0)).append("\n");
            builder.append("Name:").append(cursor.getString(1)).append("\n");
            builder.append("Telephone:").append(cursor.getString(2)).append("\n");
            builder.append("-----------------------------------\n");
        }
        showMessage("User Records",builder.toString());
    }
    public boolean selectoneByname(String name){
        Cursor cursor = db.rawQuery("SELECT * FROM USER WHERE user_name like ?",new String[]{"%"+name+"%"});
        StringBuilder  builder = new StringBuilder();
        while (cursor.moveToNext()){
            builder.append("User ID:").append(cursor.getString(0)).append("\n");
            builder.append("Name:").append(cursor.getString(1)).append("\n");
            builder.append("Telephone:").append(cursor.getString(2)).append("\n");
            builder.append("-----------------------------------\n");
        }
        if (cursor.getCount()>0) {
            showMessage("User Records", builder.toString());
            return true;
        }
        return false;
    }
    public void showMessage(String title, String message){
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .show();
    }
    public void speak(View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        startActivityForResult(intent,VOICE_REC_RESULT);
    }
}