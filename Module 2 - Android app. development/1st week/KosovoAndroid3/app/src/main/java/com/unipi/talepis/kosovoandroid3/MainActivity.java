package com.unipi.talepis.kosovoandroid3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button button2, button3, button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button2.setText("Press me!");
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Hello from button2",Toast.LENGTH_LONG).show();
            }
        });
        //button2.setOnClickListener((view -> Toast.makeText(getApplicationContext(),"Hello from button2",Toast.LENGTH_LONG).show()));
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
    }
    public void go1(View view){
        Toast.makeText(this,"Hello everyone",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button3)
            Toast.makeText(getApplicationContext(), "Hello everyone from button3", Toast.LENGTH_LONG).show();
        if (view.getId() == R.id.button4)
            startActivity(new Intent(this, MapsActivity.class));
    }
}