package com.unipi.talepis.kosovoandroid3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    SQLiteDatabase db;
    EditText editText1,editText2,editText3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editText1 = findViewById(R.id.editTextTextPersonName1);
        editText2 = findViewById(R.id.editTextTextPersonName2);
        editText3 = findViewById(R.id.editTextTextPersonName3);
        db = openOrCreateDatabase("DBUSERS", Context.MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS USER(user_id TEXT,user_name TEXT,user_tel TEXT)");
    }
    public void insert(View view){
        db.execSQL("INSERT INTO USER VALUES('"+editText1.getText()+"','"+editText2.getText()+"','"+editText3.getText()+"')");
        Toast.makeText(this,"Record inserted successfully!",Toast.LENGTH_LONG).show();
    }
    public void insertV2(View view){
        String id = editText1.getText().toString();
        String name = editText2.getText().toString();
        String tel = editText3.getText().toString();
        db.execSQL("INSERT INTO USER VALUES(?,?,?)",new String[]{id,name,tel});
        Toast.makeText(this,"Record inserted successfully!",Toast.LENGTH_LONG).show();
    }
    public void select(View view){
        Cursor cursor = db.rawQuery("SELECT * FROM USER WHERE user_id!=?",new String[]{"0"});
        StringBuilder  builder = new StringBuilder();
        while (cursor.moveToNext()){
            builder.append("User ID:").append(cursor.getString(0)).append("\n");
            builder.append("Name:").append(cursor.getString(1)).append("\n");
            builder.append("Telephone:").append(cursor.getString(2)).append("\n");
            builder.append("-----------------------------------\n");
        }
        showMessage("User Records",builder.toString());
    }
    public void selectone(View view){
        String id = editText1.getText().toString();
        Cursor cursor = db.rawQuery("SELECT * FROM USER WHERE user_id=?",new String[]{id});
        StringBuilder  builder = new StringBuilder();
        while (cursor.moveToNext()){
            builder.append("User ID:").append(cursor.getString(0)).append("\n");
            builder.append("Name:").append(cursor.getString(1)).append("\n");
            builder.append("Telephone:").append(cursor.getString(2)).append("\n");
            builder.append("-----------------------------------\n");
        }
        showMessage("User Records",builder.toString());
    }
    public void showMessage(String title, String message){
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(title)
                .setMessage(message)
                .show();
    }
}