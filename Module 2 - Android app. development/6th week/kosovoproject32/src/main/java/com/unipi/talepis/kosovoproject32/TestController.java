package com.unipi.talepis.kosovoproject32;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/user")
    public String index(){
        return "Hello authenticated user!";
    }
    @RequestMapping("/notsecure")
    public String notsec(){
        return "Free for all users!";
    }
}
