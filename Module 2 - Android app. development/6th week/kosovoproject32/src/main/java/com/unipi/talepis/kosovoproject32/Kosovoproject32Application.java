package com.unipi.talepis.kosovoproject32;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kosovoproject32Application {

    public static void main(String[] args) {
        SpringApplication.run(Kosovoproject32Application.class, args);
    }

}
