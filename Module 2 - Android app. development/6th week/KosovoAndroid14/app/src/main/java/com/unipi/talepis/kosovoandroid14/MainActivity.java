package com.unipi.talepis.kosovoandroid14;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    RequestQueue requestQueue;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        requestQueue = Volley.newRequestQueue(this);
        editText = findViewById(R.id.editTextTextPersonName);
    }
    public void fetchall(View view){
        String url = "http://10.0.2.2:8081/fetchsmartphones";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        StringBuilder builder = new StringBuilder();
                        JSONObject jo;
                        for(int i=0;i<response.length();i++){
                            try {
                                jo = response.getJSONObject(i);
                                builder.append("Model:").append(jo.get("model")).append("\n");
                                builder.append("Price:").append(jo.get("price")).append("\n");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        textView.setText(builder.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textView.setText(error.getLocalizedMessage());
                    }
                });
        requestQueue.add(jsonArrayRequest);
    }
    public void fetchone(View view){
        String id = editText.getText().toString();
        String url = "http://10.0.2.2:8081/smartphone/"+id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            textView.setText("Description:"+response.get("description"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(jsonObjectRequest);
    }
    public void postone(View view){
        String url = "http://10.0.2.2:8081/newsmartphone";
        JSONObject body = new JSONObject();
        try {
            body.put("id","9");
            body.put("model","Nokia");
            body.put("description","An old yet nice phone");
            body.put("price",125);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, body,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            textView.setText(response.get("message").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue.add(postRequest);
    }
}