package com.unipi.talepis.kosovoproject6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {
    LocationManager manager;
    TextView textView, textView2, textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
    }

    public void gps(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);
            return;
        }
        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    private String getAddressFromLocation(double latitude, double longitude){
        StringBuilder builder = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude,1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            builder.append(address).append("\n").append(city).append("\n").append(country).append("\n").append(postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
    public void notify(View view){
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this,"246")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("My first Notification")
                .setContentText("Hello students of Kosovo!");
        Intent intent = new Intent(this,MainActivity2.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this,123,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        manager.notify(567,builder.build());
    }

    @Override
    public void onLocationChanged(Location location) {
        textView.setText(String.valueOf(location.getLatitude())+","+String.valueOf(location.getLongitude()));
        textView2.setText(getAddressFromLocation(location.getLatitude(),location.getLongitude()));
        Location newlocation = new Location("home");
        newlocation.setLatitude(37.8746611);
        newlocation.setLongitude(23.7462022);
        double distance = location.distanceTo(newlocation);
        double speed = location.getSpeed();
        textView3.setText("Distance to home:"+String.valueOf(distance)+"\nCurrent Speed:"+String.valueOf(speed));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}