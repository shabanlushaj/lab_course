package com.unipi.talepis.kosovoandroid5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private static final int LOCATION_PERM_REQ = 1234;
    LocationManager manager;
    TextView textView2;
    double latitude,longitude;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_1,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.choice1:
                Toast.makeText(this,"You selected 1",Toast.LENGTH_LONG).show();
                return true;
            case R.id.choice2:
                Toast.makeText(this,"You selected 2",Toast.LENGTH_LONG).show();
                return true;
            case R.id.choice3:
                Intent intent = new Intent(this,MapsActivity.class);
                intent.putExtra("latitude",latitude);
                intent.putExtra("longitude",longitude);
                startActivity(intent);
                return true;
            default:
                return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView2 = findViewById(R.id.textView2);
        manager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    public void go2(View view) {
        EditText editText = findViewById(R.id.editTextTextPersonName);
        Intent intent = new Intent(this, MainActivity2.class);
        intent.putExtra("message", editText.getText().toString());
        startActivity(intent);
    }

    private void openGPS() {
        //Toast.makeText(this,"GPS opened!",Toast.LENGTH_LONG).show();

    }
    private void stopGPS(){
        manager.removeUpdates(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //if (requestCode == LOCATION_PERM_REQ) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat
                        .checkSelfPermission(this,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                }
                manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            //}
        }
    }

    public void gps(View view){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=
                PackageManager.PERMISSION_GRANTED){
            //request it
            //Toast.makeText(this,"You don't have Location Permission...",Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERM_REQ);
        }else {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
            //you already have the permission
            //Toast.makeText(this,"You have Location Permission!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        textView2.setText(String.valueOf(location.getLatitude())+","+String.valueOf(location.getLongitude()));
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}